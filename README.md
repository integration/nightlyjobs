# Moodle Jenkins Pipeline DSL Configuration

## Examples

### PHPUnit + Postgres on Integration/Main on highest supported version of PHP

```
@Library('ci') _

runTask(new org.moodle.ci.Task(
    task: "phpunit",
    phpVersion: org.moodle.ci.Task.HIGHEST,
    database: "pgsql",
    moodleVersion: new org.moodle.ci.versions.Main(),
    repository: new org.moodle.ci.repositories.Integration()
))
```

### PHPUnit + MariaDB 10.7 on Integration/4.0 on PHP 8.0

```
@Library('ci') _

runTask(new org.moodle.ci.Task(
    task: "phpunit",
    phpVersion: "8.0",
    database: "mariadb",
    databaseTag: "10.7",
    moodleVersion: new org.moodle.ci.versions.M400(),
    repository: new org.moodle.ci.repositories.Integration()
))
```

### Behat + OCI on Integration/Main with Chrome

```
@Library('ci') _

runTask(new org.moodle.ci.Task(
    task: "behat",
    database: "oci",
    moodleVersion: new org.moodle.ci.versions.Main(),
    repository: new org.moodle.ci.repositories.Integration(),
    browser: new org.moodle.ci.browsers.Chrome()
))
```

### Behat + MSSQL on Security/4.3 (non-JS)

```
@Library('ci') _

runTask(new org.moodle.ci.Task(
    task: "behat",
    database: "sqlsrv",
    moodleVersion: new org.moodle.ci.versions.M403(),
    repository: new org.moodle.ci.repositories.Security(),
    browser: new org.moodle.ci.browsers.BrowserKit()
))
```
