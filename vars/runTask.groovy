def call(org.moodle.ci.Task task) {
    pipeline {
        agent {
            label "${task.getNodeLabel()}" ?: "docker"
        }

        environment {
            PHP_VERSION = "${task.phpVersion}"
            DBTYPE = "${task.database}"
            DBTAG = "${task.databaseTag}"
            DBREPLICAS = "${task.databaseReplicaCount}"
            JOBTYPE = "${task.task}"
            GOOD_COMMIT = "${task.getGoodCommit()}"
            BAD_COMMIT = "${task.getBadCommit()}"
            PATHTORUNNER = "${HOME}/mainscripts/runner"
            PATHTOTESTRUNNER = "${HOME}/testscripts/runner"
            BROWSER = "${task.browserName}"
            BROWSER_HEADLESS = "${task.browserHeadless}"
            BROWSER_CHROME_ARGS = "${task.browserChromeArgs}"
            BROWSER_FIREFOX_ARGS = "${task.browserFirefoxArgs}"
            BROWSER_DEBUG = "${task.browserDebug}"
            MOBILE_VERSION = "${task.getMobileVersion(env)}"
            PLUGINSTOINSTALL= task.getPluginsToInstall()
            MOODLE_CONFIG = task.getMoodleConfigJSON()
            PHPUNIT_TESTSUITE = "${task.phpunitTestsuite}"
            PHPUNIT_FILTER = "${task.phpunitFilter}"
            MLBACKEND_PYTHON_VERSION = "${task.mlbackendPythonVersion}"
            BEHAT_SUITE = "${task.behatSuite}"
            BEHAT_TAGS = "${task.behatTags}"
            BEHAT_NAME = "${task.behatName}"
            BEHAT_PARALLEL = "${task.behatParallel}"
            BEHAT_RERUNS = "${task.behatReruns}"
            BEHAT_TIMING_FILENAME = "${task.timingFileName}"
            BEHAT_INCREASE_TIMEOUT = "${task.behatTimeoutMultiplier}"
            MOBILE_APP_IMAGE = "moodlehq/moodleapp:${task.getMobileVersion(env)}"
            MOBILE_APP_PORT = "${task.getMobileAppPort()}"
            SELVERSION = "${task.seleniumVersion}"
            USE_SELVERSION = "${task.useSeleniumVersion}"
        }

        stages {
            stage("Git Checkout") {
                steps {
                    checkout(
                        [
                            $class: 'GitSCM',
                            branches: [
                                [
                                    name: "${task.branch}"
                                ]
                            ],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [
                                [
                                    $class: 'CloneOption',
                                    depth: 1,
                                    noTags: false,
                                    reference: "${env.HOME}/caches/integration.git",
                                    shallow: true
                                ],
                                [
                                    $class: 'RelativeTargetDirectory',
                                    relativeTargetDir: 'moodle'
                                ]
                            ],
                            submoduleCfg: [],
                            userRemoteConfigs: [
                                [
                                    url: "${task.url}",
                                    credentialsId: "${task.credentialsId}"
                                ]
                            ]
                        ]
                    )
                }
            }

            stage("Build moodleapp") {
                when {
                    expression {
                        return task.hasMobileAppBuild();
                    }
                }
                steps {
                    checkout(
                        [
                            $class: 'GitSCM',
                            branches: [
                                [
                                    name: "${task.mobileAppBuildBranch}"
                                ]
                            ],
                            extensions: [
                                [
                                    $class: 'CloneOption',
                                    depth: 0,
                                    noTags: false,
                                    shallow: true
                                ],
                                [
                                    $class: 'RelativeTargetDirectory',
                                    relativeTargetDir: 'mobileapp'
                                ]
                            ],
                            submoduleCfg: [],
                            userRemoteConfigs: [
                                [
                                    url: "git@github.com:moodlemobile/moodleapp.git",
                                    credentialsId: "${task.credentialsId}"
                                ]
                            ]
                        ]
                    )
                    script {
                        docker.build(env.MOBILE_APP_IMAGE, "--build-arg build_command='npm run build:test' mobileapp")
                    }
                }
            }

            stage("Run Task") {
                steps {
                    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
                        cache(
                            maxCacheSize: 250,
                            caches: [
                                [$class: 'ArbitraryFileCache', excludes: '', includes: '*.json', path: "timing"]
                            ]
                        ) {
                          // It is not possible to use the registry with DSL yet.
                          script {
                            docker.withRegistry('', 'dockerhub') {
                              sh task.getPathToRunner(env, steps)
                            }
                          }
                        }
                    }
                }
            }

        }
        post {
            always {
                // Process the jUnit test results.
                // (note that behat saves them as .xml files under junit subdir, and
                // phpunit saves them as .junit files without subdir, hence the 2 patterns)
                // By default only process last-rerun junit files. Those are the final, consistent failures.
                script {
                    // By default, process all the junit files.
                    def jFiles = "${env.BUILD_ID}/*.junit/*.xml,${env.BUILD_ID}/*.junit"
                    if (task.task == "behat" && task.behatReruns > 0) {
                        // For behat tasks with reruns, let's process last rerun junit logs only.
                        jFiles = "${env.BUILD_ID}/*rerun${task.behatReruns}.junit/*.xml"
                    }
                    junit allowEmptyResults: true,
                        testResults: "${jFiles}"
                }

                // Keep a copy of the junit artifacts into s3 bucket.
                // (note that behat saves them as .xml files under junit subdir, and
                // phpunit saves them as .junit files without subdir, hence the 2 patterns)
                step([
                    $class: 'hudson.plugins.s3.S3BucketPublisher',
                    consoleLogLevel: 'INFO',
                    dontSetBuildResultOnFailure: false,
                    dontWaitForConcurrentBuildCompletion: false,
                    entries: [
                        [
                            bucket: 'moodle-integration-ci',
                            excludedFile: '',
                            flatten: false,
                            gzipFiles: false,
                            keepForever: true,
                            managedArtifacts: true,
                            noUploadOnFailure: false,
                            selectedRegion: 'eu-west-1',
                            showDirectlyInBrowser: true,
                            sourceFile: "${env.BUILD_ID}/*.junit/*.xml,${env.BUILD_ID}/*.junit",
                            storageClass: 'STANDARD',
                            uploadFromSlave: true,
                            useServerSideEncryption: false
                        ]
                    ],
                    pluginFailureResultConstraint: 'SUCCESS',
                    profileName: 'Integration Team',
                    userMetadata: []
                ])

                // Keep all artifacts in the build directory, but don't store the junit files. These are in S3 anyway.
                // (note that behat saves them as .xml files under junit subdir, and
                // phpunit saves them as .junit files without subdir, hence the 2 patterns)
                archiveArtifacts allowEmptyArchive: true,
                    artifacts: "${env.BUILD_ID}/**",
                    excludes: "${env.BUILD_ID}/*.junit/*.xml,${env.BUILD_ID}/*.junit"

                // Clean the workspace. No point storing all of this.
                cleanWs deleteDirs: true, notFailBuild: true

                // Notify via our custom Telegram notifier.
                script {
                    org.moodle.ci.Notifier.notify(task, currentBuild)
                }
            }
        }
    }
}

return this
