def call(org.moodle.ci.Task task) {
  pipeline {
    agent {
      label "${task.getNodeLabel()}" ?: "docker"
    }

    environment {
      PHP_VERSION = "${task.phpVersion}"
      GIT_BRANCH = "${task.branch}"
      DBTYPE = "${task.database}"
      DBTAG = "${task.databaseTag}"
      DBREPLICAS = "${task.databaseReplicaCount}"
      JOBTYPE = "${task.task}"
      PATHTORUNNER = "${HOME}/mainscripts/runner"
      PATHTOTESTRUNNER = "${HOME}/testscripts/runner"
      LOCAL_CI_PATH = "${HOME}/tools/local_ci"
      PLUGINSTOINSTALL= task.getPluginsToInstall()
      MOODLE_CONFIG = task.getMoodleConfigJSON()
    }

    stages {
      stage("Git Checkout") {
        steps {
          // We need to enclose this into script step to be able to capture
          // all the scm variables and add them to the environment.
          script {
            def Map scmVars = checkout(
              [
                $class: 'GitSCM',
                branches: [
                  [
                    name: "${task.branch}"
                  ]
                ],
                doGenerateSubmoduleConfigurations: false,
                extensions: [
                  [
                    $class: 'LocalBranch',
                    localBranch: "${task.branch}"
                  ],
                  [
                    $class: 'CloneOption',
                    depth: 0,
                    noTags: false,
                    reference: "${env.HOME}/caches/integration.git",
                    shallow: false
                  ],
                  [
                    $class: 'RelativeTargetDirectory',
                    relativeTargetDir: 'moodle'
                  ]
                ],
                submoduleCfg: [],
                userRemoteConfigs: [
                  [
                    url: "${task.url}",
                    credentialsId: "${task.credentialsId}"
                  ]
                ]
              ]
            )
            // Add all the scm variables to the environment.
            // We'll use a few like GIT_COMMIT, GIT_BRANCH, GIT_PREVIOUS_COMMIT, ...
            scmVars.each { k, v -> env[k] = v }
          }
        }
      }

      stage("Run Task") {
        steps {
          wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'xterm']) {
            // Copy all the artifacts from the previous build, some of the script
            // do use them for better behaviour and or tracking changes along the time.
            copyArtifacts projectName: "${env.JOB_NAME}",
              selector: lastCompleted(),
              target: "${env.BUILD_ID}",
              filter: "**/*.txt,**/*.csv",
              flatten: true,
              optional: true
            // It is not possible to use the registry with DSL yet.
            script {
              docker.withRegistry('', 'dockerhub') {
                sh task.getPathToRunner(env, steps)
              }
            }
          }
        }
      }
    }

    post {
      always {
        // Keep all artifacts in the build directory.
        archiveArtifacts allowEmptyArchive: true,
          artifacts: "${env.BUILD_ID}/**"

        // Clean the workspace. No point storing all of this.
        cleanWs deleteDirs: true, notFailBuild: true

        // Notify via our custom matrixbot notifier.
        script {
          org.moodle.ci.Notifier.notify(task, currentBuild)
        }
      }
    }
  }
}

return this
