package org.moodle.ci

import org.moodle.ci.repositories.Repository
import org.moodle.ci.versions.Version
import org.moodle.ci.browsers.Browser
import groovy.json.JsonException
import groovy.json.JsonOutput
import groovy.json.JsonSlurperClassic
import java.security.*

/**
 * A Moodle Continuous Integration Task to be run.
 */
class Task {
    /**
     * A value representing the highest possible version to use.
     * Used in combination with phpVersion.
     */
    static final HIGHEST = "HIGHEST"

    /**
     * A value representing the lowest possible version to use.
     * Used in combination with phpVersion.
     */
    static final LOWEST = "LOWEST"

    /**
     * A value representing the stable version.
     * Used in combination with runnerVersion.
     */
    static final STABLE = "STABLE"

    /**
     * A value representing the testing version.
     * Used in combination with runnerVersion..
     */
    static final TESTING = "TESTING"

    /**
     * The Repository under test.
     */
    protected Repository repository

    /**
     * The version of Moodle under test.
     */
    protected Version moodleVersion

    /**
     * The Browser being tested (for Behat).
     */
    protected Browser browser

    /**
     * Additional browser arguments when using Chrome (for Behat).
     */
    protected String browserChromeArgs = ""

    /**
     * Additional browser arguments when using Firefox (for Behat).
     */
    protected String browserFirefoxArgs = ""

    /**
     * The Repository URL of the branch under test.
     */
    protected String url

    /**
     * The branch under test.
     */
    protected String branch

    /**
     * The Name of the Jenkins Credentials set to use.
     */
    protected String credentialsId

    /**
     * The Database to test against.
     */
    protected String database = 'pgsql'

    /**
     * The database version (docker tag) to test against. Optional.
     */
    protected String databaseTag = '';

    /**
     * The number of database replicas to include.
     */
    protected Integer databaseReplicaCount = 0

    /**
     * The version of PHP to test against.
     */
    protected String phpVersion

    /**
     * The task to run.
     */
    protected String task = 'phpunit'

    /**
     * The notification URL.
     *
     * Note: This can be a Boolean, so should not be type-hinted until we fix this.
     */
    protected notify = "https://matrixbot.moodle.org/jenkinsnotify"

    /**
     * An path override for the runner.
     */
    protected String pathToRunner

    /**
     * A Behat 'suite' equates to a Moodle theme.
     */
    protected String behatSuite = ""

    /**
     * The list of Behat tags.
     */
    protected String behatTags

    /**
     * The Behat feature or scenario name to be run.
     */
    protected String behatName

    /**
     * The Moodle app version to be run.
     */
    protected String mobileVersion

    /**
     * The Moodle port for the mobile app to be run.
     *
     * Note that this gets precedence over the automatic
     * port calculations applied when mobileBuildAppBranch is set.
     */
    protected Integer mobileAppPort

    /**
     * Mobile App Branch to build.
     */
    protected String mobileAppBuildBranch = ""

    /**
     * The version of Selenium image to use.
     */
    protected String seleniumVersion = "latest"

    /**
     * Whether to use the selenium version (only necessary when using Chrome).
     */
    protected Boolean useSeleniumVersion = false

    /**
     * External plugins to install.
     */
    protected moodlePlugins = []

    /**
     * Custom configuration settings to apply.
     */
    protected moodleConfig = [:]

    /**
     * A testsuite to restrict PHPUnit runs to.
     */
    protected String phpunitTestsuite

    /**
     * A filter to restrict PHPUnit runt to.
     */
    protected String phpunitFilter

    /**
     * The mlbackend-python tag (from docker hub) to run the analytics tests.
     */
    protected String mlbackendPythonVersion

    /**
     * The version of the runner to use.
     * Options are STABLE and TESTING
     */
    protected String runnerVersion = STABLE

    /**
     * The number of Behat parallel jobs to run.
     */
    protected Integer behatParallel = 3

    /**
     * The number of reruns to perform with Behat failed runs.
     *
     * Defaults to 1 for all our jobs if not specified via Task param. 0 means no reruns.
     */
    protected Integer behatReruns = 1

    /**
     * The git ref (commit, tag, branch) to be used when running bisect sessions as good commit.
     *
     * Defaults to empty that means run the standard job, without any bisect session.
     */
    protected String goodCommit

    /**
     * The git ref (commit, tag, branch) to be used when running bisect sessions as bad commit.
     *
     * Defaults to empty that means run the standard job, without any bisect session.
     */
    protected String badCommit

    /**
     * The label to use to run the task, this can be used to force a worker.
     *
     * The most obvious case is to be able to select a specific node/worker to run a job
     * (worker01...workerXY), but can be also used to specify different nodes/workers
     * depending of the task to be run.
     *
     * It defaults to "docker" if not specified.
     */
    protected String nodeLabel = "docker"

     /**
      * The behat timeout multiplier.
      *
      * By default behat uses a timeout of 10s. Setting this to any value will get that multiplied
      * so 2 means 10 * 2, 3 means 30 and so on. 0 means no multiplier is applied and it's the default.
      */
    protected Integer behatTimeoutMultiplier = 0

    /**
     * The URL of the repository to fetch from.
     *
     * @return String
     */
    String getUrl() {
        if (url) {
            if (url.startsWith("git://github.com")) {
                url = url.replace("git://github.com", "https://github.com")
            }

            return url
        }

        if (repository) {
            return repository.url
        }

        throw new Exception('No repository specified.')
    }

    /**
     * The Name of the Jenkins Credentials set to use.
     */
    String getCredentialsId() {
        if (credentialsId) {
            return credentialsId
        }

        if (repository && repository.credentialsId) {
            return repository.credentialsId
        }

        return null
    }

    /**
     * The Branch under test.
     */
    String getBranch() {
        if (branch) {
          return branch
        }

        if (moodleVersion) {
          return moodleVersion.defaultBranch
        }

        throw new Exception('No Pull URL set')
    }

    /**
     * The PHP Version to test against.
     */
    String getPhpVersion() {
        if (phpVersion) {
            if (phpVersion == HIGHEST) {
                return moodleVersion.getHighestSupportedVersion()
            }
            if (phpVersion == LOWEST) {
                return moodleVersion.getLowestSupportedVersion()
            }
            return phpVersion
        }

        if (moodleVersion) {
            return moodleVersion.getHighestSupportedVersion()
        }

        throw new Exception("I don't know what PHP Version to test against")
    }

    /**
     * The parallelism to use for Behat.
     *
     * Forced to be between 1 and 10
     */
    def getBehatParallel() {
        if (behatParallel < 1) {
            return 1
        }
        if (behatParallel > 10) {
            return 10
        }
        return behatParallel
    }

    /**
     * The number of reruns to perform with Behat failed runs
     *
     * Forced to be between 0 (no reruns) and 9.
     */
    def getBehatReruns() {
        if (behatReruns < 0) {
            return 1
        }
        if (behatReruns > 9) {
            return 9
        }
        return behatReruns
    }

    /**
     * The reference to be used in a bisect session as good commit.
     */
    def getGoodCommit() {
        return goodCommit ?: ''
    }

    /**
     * The reference to be used in a bisect session as bad commit.
     */
    def getBadCommit() {
        return badCommit ?: ''
    }

    /**
     * The node label to be used for the task
     */
    def getNodeLabel() {
      return nodeLabel ?: 'docker';
    }

    /**
     * The timeout multiplier to be applied to behat standard timeout
     *
     * Can return zero or anything > 1 (1 as multiplier is silly and negatives too).
     */
    def getBehatTimeoutMultiplier() {
        if (behatTimeoutMultiplier < 0) {
            return 0
        }
        if (behatTimeoutMultiplier == 1) {
            return 0
        }
        return behatTimeoutMultiplier
    }

    def getPathToRunner(env, steps) {
        def runner
        if (pathToRunner) {
            runner = new File(pathToRunner)
            if (!runner.exists()) {
                throw new Exception("Unable to find job runner at ${pathToRunner}")
            }

            return runner.getAbsolutePath()
        }

        def runnerBase = "${env.PATHTORUNNER}"
        if (runnerVersion == TESTING) {
            runnerBase = "${env.PATHTOTESTRUNNER}"
        }
        if (moodleVersion) {
            runner = new File("${runnerBase}/${moodleVersion.name}/run.sh")
            if (!runner.exists()) {
                runner = new File("${runnerBase}/main/run.sh")
            }

            return runner.getAbsolutePath()
        } else {
            def versionName = steps.sh (
                script: 'grep "\\$branch" moodle/version.php | sed "s/^[^\']*\'\\([^\']*\\).*\$/\\1/"',
                returnStdout: true
            ).trim()
            runner = new File("${runnerBase}/${versionName}/run.sh")

            if (!runner.exists()) {
                runner = new File("${runnerBase}/main/run.sh")
            }

            return runner.getAbsolutePath()
        }

        throw new Exception("Unable to determine path to script runner.")
    }

    /**
     * Whether the Task should trigger a notification.
     *
     * @return  Whether the task you notify or not
     */
    Boolean shouldNotify() {
        if (false == notify) {
            return false
        }

        if (null == notify) {
            return false
        }

        if ('' == notify) {
            return false
        }

        return true
    }

    def getNotify() {
        return notify
    }

    String getBrowserName() {
        if (browser) {
            return browser.getName()
        }

        return ""
    }

    Integer getBrowserHeadless() {
        if (browser && browser.headless) {
            return 1
        }

        return 0
    }

    Integer getBrowserDebug() {
        if (browser && browser.debug) {
            return 1
        }

        return 0
    }

    /**
     * Get the name of the Behat suite (Moodle theme) to test with.
     *
     * @param   nameDefault Whether to include a name for the default theme
     * @return  The name of the Behat suite
     */
    String getBehatSuiteName(String defaultName = "") {
        if (this.behatSuite) {
            return this.behatSuite
        }

        return defaultName
    }

    /**
     * Get the tags to run Behat against.
     *
     * @return  The tags, or default tags for a specific browser.
     */
    def getBehatTags() {
        if (behatTags) {
            return behatTags
        }

        if (browser) {
            return browser.getDefaultTags()
        }

        return ""
    }

    /**
     * Get the name to run Behat against.
     *
     * @return The Behat feature or scenario name.
     */
    def getBehatName() {
        if (behatName) {
            return behatName
        }

        return ""
    }

    /**
     * Get the Moodle app version to run the Behat tests with the @app tag.
     *
     * @return The Moodle app version to be run.
     */
    def getMobileVersion(env) {
        if (mobileVersion) {
            return mobileVersion
        }

        if (hasMobileAppBuild()) {
            return "integration_${env.BUILD_ID}"
        }

        return ""
    }

    /**
     * Add a plugin to install.
     *
     * @param  newPlugin The new plugin to install.
     */
    def addMoodlePlugin(MoodlePlugin newPlugin) {
        moodlePlugins << newPlugin
    }

    /**
     * Get the external plugins to install in the site.
     *
     * @return The external plugins list to install. The expected format is defined in run.sh from the moodle-ci-runner.
     */
    def getPluginsToInstall() {
        def pluginString = []

        moodlePlugins.each {
            pluginString.push(it.getPluginAsString())
        }

        return pluginString.join(';')
    }

    /**
     * Add a configuration setting to apply.
     */
    def addMoodleConfig(String key, String value) {
        moodleConfig[key] = value
    }

    /**
     * Add configuration settings (1..n) from JSON.
     *
     * The json string must be a valid name:value json object/associative array.
     */
    def addMoodleConfigFromJSON(String json) {
        // Only if there is any json defined.
        if (json) {
            try {
                // Parse the config. We are using the classic parser to avoid the LazyMap object.
                // because it cannot be used in Jenkins pipeline (not serializable).
                def newConfig = new JsonSlurperClassic().parseText(json)
                // Add the new config to the current one.
                moodleConfig += newConfig
            } catch (JsonException e) {
                // Do nothing.
            }
        }
    }

    /**
     * Get the configuration settings to apply in JSON format.
     *
     * @return The configuration settings to apply in JSON format. The expected format is defined in moodle-ci-runner.
     *         (basically a JSON string being a map object of single {name: value} pairs).
     */
    def getMoodleConfigJSON() {
        def jsonConfig = ""

        if (!moodleConfig.isEmpty()) {
            // Added prettify to make it easier to read and check that the runner supports multi line values.
            jsonConfig = JsonOutput.prettyPrint(JsonOutput.toJson(moodleConfig))
        }

        return jsonConfig
    }

    /**
     * Get the PHPUnit testsuite to be run.
     *
     * @return  The PHPUnit suite, or list of suites, to run.
     */
    def getPhpunitTestsuite() {
        if (phpunitTestsuite) {
            return phpunitTestsuite
        }

        return ""
    }

    /**
     * Get the PHPUnit filter to be run.
     *
     * @return  The PHPUnit filter to run.
     */
    def getPhpunitFilter() {
        if (phpunitFilter) {
            return phpunitFilter
        }

        return ""
    }

    /**
     * Get the mlbackend-python docker tag to be used. Empty means don't run those analytics tests.
     *
     * @return The docker tag to be used.
     */
    def getMlbackendPythonVersion() {
        if (mlbackendPythonVersion) {
            return mlbackendPythonVersion
        }

        return ""
    }

    /**
     * Get the name of the behat timing file.
     *
     * @return  The timing file name
     */
    String getTimingFileName() {
        return sprintf('%s.json', getBehatSuiteName('default'))
    }

    /**
     * Get the SHA1 hash of a string.
     *
     * @param   value The value to be hashed
     * @return        The calculated value
     */
    String getShaSum(String value) {
        MessageDigest md = MessageDigest.getInstance("SHA1")

        return md.digest(value.getBytes()).encodeHex()
    }

    /**
     * Get a summary of the build.
     *
     * @return  A brief summary of the Task.
     */
    String getBuildDescription() {
        def databaseDescription = database;
        if (databaseTag) {
            databaseDescription += ":${databaseTag}"
        }

        def appSuffix = ""
        if (mobileVersion) {
            appSuffix = " - app"
        }

        def bisectSuffix = ""
        if (goodCommit || badCommit) {
            bisectSuffix = " - bisect"
        }

        return getBranch() + ": ${task} ${databaseDescription} (PHP" + getPhpVersion() + ")" + appSuffix + bisectSuffix
    }

    /**
     * Get the number of database replicas.
     *
     * @return The number of database replicas
     */
    Integer getDatabaseReplicaCount() {
        if (databaseReplicaCount < 0) {
            return 0
        }
        if (databaseReplicaCount > 1) {
            return 1
        }
        return databaseReplicaCount
    }

    /**
     * Get the branch name of the moodle mobile app to build.
     *
     * This is the branch of the actual app to build with `docker build`.
     *
     * @return The name of the branch
     */
    String getMobileAppBuildBranch() {
        return mobileAppBuildBranch
    }

    /**
     * Whether the mobile app should be built.
     *
     * @return Whether it should be built
     */
    Boolean hasMobileAppBuild() {
        return mobileAppBuildBranch != ""
    }

    Integer getMobileAppPort() {
        if (mobileAppPort) { // Gets precedence.
          return mobileAppPort
        }
        if (hasMobileAppBuild()) { // All built apps run on port 80.
            return 80
        }

        return 8100 // Old ionic3 builds were the default.
    }

    Boolean getUseSeleniumVersion() {
        if (seleniumVersion.startsWith("3") && browser.name.startsWith("chrome")) {
            return false
        } else {
            return true
        }
    }
}
