package org.moodle.ci.github

import hudson.EnvVars
import hudson.model.*
import groovy.json.JsonSlurper
import jenkins.model.*

/**
 * This task looks for all the branches in the https://github.com/moodlehq/moodle-php-apache repository
 * that have the trigger_new_builds.yml workflow defines and, with the frequency specified (1 of X executions)
 * it creates a workflow_dispatch event in the repository, so the workflow in executed, ultimately
 * deciding if a new rebuild of the docker image is needed or no.
 *
 * Note this makes extensive use of the github API to a number of actions:
 * - Fetch all the branches in the repository
 * - Look if the trigger_new_builds.yml file exists.
 * - Launch the trigger_new_builds.yml workflow for the target branches.
 */
class PhpDockerImagesWorkflowDispatch {

  /**
   * Jenkins view (exact matching) that contains the jobs to be examined.
   */
  protected String ghToken = null

  /**
   * Job build number, it's used as seed to calculate which PHP branches will get the workflow_dispatch
   * event. If not set a random integer will be used instead.
   */
  protected int buildNumber = 0

  /**
   * How many executions of the script are needed to get a workflow dispatch event generated for a branch.
   */
  protected int frequency = 3

  /**
   * Run the configured dispatch actions.
   */
  int run(Script out) {
    // Let's fetch all the branches of the moodlehq/moodle-php-apache repo
    def req = new URL("https://api.github.com/repos/moodlehq/moodle-php-apache/" +
      "branches?per_page=100").openConnection()
    req.setRequestMethod("GET")
    req.setRequestProperty("Accept", "application/vnd.github+json")
    req.setRequestProperty("X-GitHub-Api-Version", "2022-11-28")
    req.setRequestProperty("Authorization", "Bearer " + this.ghToken)
    if (!req.getResponseCode().equals(200)) {
      out.println("Problems fetching the list of branches from GH.")
      return 1
    }

    // Filter the information to get just the list of candidate branch names.
    def list = new JsonSlurper().parseText(req.getInputStream().getText())
    def branches = []
    list.each {
      branches.add(it.name)
    }

    // Let's verify, for each branch, if they have the trigger_new_builds workflow.
    // (we are going to use that to decide if the branch is actionable or no)
    for (int i = 0; i < branches.size(); i++){
      req = new URL("https://api.github.com/repos/moodlehq/moodle-php-apache/" +
        "contents/.github/workflows/trigger_new_builds.yml?ref=" + branches[i]).openConnection()
      req.setRequestMethod("HEAD")
      req.setRequestProperty("Accept", "application/vnd.github+json")
      req.setRequestProperty("X-GitHub-Api-Version", "2022-11-28")
      req.setRequestProperty("Authorization", "Bearer " + this.ghToken)
      if (req.getResponseCode().equals(200)) {
        // All ok, the file exists for the branch, so we keep it.
      } else if (req.getResponseCode().equals(404)) {
        // No workflow, remove the branch from the list of candidates.
        branches.remove(i)
        i--
      } else {
        out.println("Problems fetching information about the " + it + "branch from GH.")
        return 1
      }
    }

    // If not buildNumber has been passed, let's calculate a random one (0-99)
    if (!this.buildNumber) {
      this.buildNumber = Math.abs(new Random().nextInt() % 100)
    }

    // Given we know the build number and the frequency, let's do some simple maths to decide
    // which branches will get the workflow_dispatch event generated. Just do it for the branches
    // which position matches the module of buildNumber % frequency.
    def int targetPosition = this.buildNumber % this.frequency
    branches.eachWithIndex { branch, idx ->
      if (idx % frequency == targetPosition) {
        // Generate the dispatch event for this php branch.
        out.println(branch + ": workflow_dispatch event generated, conditional build will happen soon.")
        def body = '{"ref":"' + branch + '"}'
        req = new URL("https://api.github.com/repos/moodlehq/moodle-php-apache/" +
          "actions/workflows/trigger_new_builds.yml/dispatches").openConnection()
        req.setRequestMethod("POST")
        req.setRequestProperty("Accept", "application/vnd.github+json")
        req.setRequestProperty("X-GitHub-Api-Version", "2022-11-28")
        req.setRequestProperty("Authorization", "Bearer " + this.ghToken)
        req.setDoOutput(true)
        req.getOutputStream().write(body.getBytes("UTF-8"))
        if (!req.getResponseCode().equals(204)) {
          println(req.getResponseCode())
          println(req.getInputStream().getText())
          out.println("Problems fetching the list of branches from GH.")
          return 1
        }
      } else {
        // Nothing to do, this won't get the event generated.
        out.println(branch + ": skipped")
      }
    }

    return 0
  }
}
