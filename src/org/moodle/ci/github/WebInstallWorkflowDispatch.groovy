package org.moodle.ci.github

import hudson.EnvVars
import hudson.model.*
import groovy.json.JsonSlurper
import jenkins.model.*

class WebInstallWorkflowDispatch {

    protected String repoName = 'moodlehq/moodle-webinstaller-test'
    protected String workflowName = 'web-installer-test.yml'
    protected String ghToken = null
    protected String repository = 'https://git.in.moodle.com/moodle/integration.git'
    protected String branch = 'main'

    String getRequestUrl() {
        return "https://api.github.com/repos/${repoName}/actions/workflows/${workflowName}/dispatches"
    }

    int run(Script out) {
        out.println("Dispatching GHA web installation testing workflow run.")

        def body = '{"ref": "main", "inputs": {"repository": "' + repository + '", "branch": "' + branch + '"}}'
        if (!makePostRequest(getRequestUrl(), body)) {
            out.println("Problems while launching GHA workflow run.")
            return 1
        }

        sleep(10000) // Wait for 10 seconds before checking the status.

        def lastRun = getLatestRun()
        if (waitForWorkflowCompletion(lastRun, out)) {
            out.println("The workflow run was successful. View details at: https://github.com/${repoName}/actions/runs/" + lastRun.id)
            return 0
        } else {
            out.println("The workflow run failed or timed out. View details at: https://github.com/${repoName}/actions/runs/" + lastRun.id)
            return 1
        }
    }

    String makeGetRequest(String urlString) {
        def req = new URL(urlString).openConnection()
        req.setRequestMethod("GET")
        req.setRequestProperty("Accept", "application/vnd.github+json")
        req.setRequestProperty("Authorization", "Bearer " + this.ghToken)
        return req.getInputStream().getText()
    }

    boolean makePostRequest(String urlString, String body) {
        try {
            def req = new URL(urlString).openConnection()
            req.setRequestMethod("POST")
            req.setRequestProperty("Accept", "application/vnd.github.v3+json")
            req.setRequestProperty("Authorization", "Bearer " + this.ghToken)
            req.setRequestProperty("Content-Type", "application/json")
            req.setDoOutput(true)
            req.getOutputStream().write(body.getBytes("UTF-8"))

            int responseCode = req.getResponseCode()
            if (responseCode != 204) {
                // Read error stream to get more details about the error
                def errorStream = req.getErrorStream().getText("UTF-8")
                println("Error Response: $errorStream") // Debugging: Log the error response from GitHub
            }

            return responseCode == 204 // GitHub API returns 204 No Content on success
        } catch (Exception e) {
            println("Error making POST request: $e.message") // Debugging: Log any exceptions
            return false
        }
    }

    def getLatestRun() {
        def runsReqUrl = "https://api.github.com/repos/${repoName}/actions/runs"
        def runsResponseText = makeGetRequest(runsReqUrl)
        def runsObject = new JsonSlurper().parseText(runsResponseText)
        return runsObject.workflow_runs.max { Date.parse("yyyy-MM-dd'T'HH:mm:ss'Z'", it.created_at) }
    }

    boolean waitForWorkflowCompletion(def lastRun, Script out) {
        def timeOutMillis = 20 * 60 * 1000 // 20 minutes.
        def startTime = System.currentTimeMillis()

        while (System.currentTimeMillis() - startTime < timeOutMillis) {
            sleep(10000) // Wait for 10 seconds before checking the status again.

            lastRun = getLatestRun() // Fetch the latest run status.
            if (lastRun.status == "completed") {
                break // Exit the loop if the workflow run has completed.
            }
        }

        if (lastRun.conclusion == "success") {
            return true // Return true if the workflow run was successful.
        } else {
            out.println("Workflow run failed with conclusion: ${lastRun.conclusion}")
            return false // Return false if the workflow run failed.
        }
    }
}
