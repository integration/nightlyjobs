package org.moodle.ci.versions

class M37 extends Version {
    M37() {
        this.name = "37"
        this.defaultBranch = "MOODLE_37_STABLE"
        this.supportedPHPVersions = [
            "7.1",
            "7.2",
            "7.3",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
