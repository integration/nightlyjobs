package org.moodle.ci.versions

class WP37 extends Version {
    WP37() {
        this.name = "37"
        this.defaultBranch = "WORKPLACE_37_LATEST"
        this.supportedPHPVersions = [
            "7.1",
            "7.2",
            "7.3",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
