package org.moodle.ci.versions

class WPR401 extends Version {
    WPR401() {
        this.name = "401"
        this.defaultBranch = "WORKPLACE_ROLLING_401_LATEST"
        this.supportedPHPVersions = [
            "7.4",
            "8.0",
            "8.1"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
