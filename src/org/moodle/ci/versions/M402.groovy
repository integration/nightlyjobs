package org.moodle.ci.versions

class M402 extends Version {
    M402() {
        this.name = "402"
        this.defaultBranch = "MOODLE_402_STABLE"
        this.supportedPHPVersions = [
            "8.0",
            "8.2",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
