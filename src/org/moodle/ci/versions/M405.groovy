package org.moodle.ci.versions

class M405 extends Version {
    M405() {
        this.name = "405"
        this.defaultBranch = "MOODLE_405_STABLE"
        this.supportedPHPVersions = [
            "8.1",
            "8.3",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
