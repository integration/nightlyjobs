package org.moodle.ci.versions

class Main extends Version {
  Main() {
    this.name = "main"
    this.defaultBranch = "main"
        this.supportedPHPVersions = [
            "8.2",
            "8.3",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
