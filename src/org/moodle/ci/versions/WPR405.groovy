package org.moodle.ci.versions

class WPR405 extends Version {
    WPR405() {
        this.name = "405"
        this.defaultBranch = "WORKPLACE_ROLLING_405_LATEST"
        this.supportedPHPVersions = [
            "8.1",
            "8.3"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
