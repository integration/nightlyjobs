package org.moodle.ci.versions

class M34 extends Version {
    M34() {
        this.name = "34"
        this.defaultBranch = "MOODLE_34_STABLE"
        this.supportedPHPVersions = [
            "7.0",
            "7.1",
            "7.2",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
