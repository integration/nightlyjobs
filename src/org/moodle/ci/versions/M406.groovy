package org.moodle.ci.versions

class M406 extends Version {
    M406() {
        this.name = "406"
        this.defaultBranch = "MOODLE_406_STABLE"
        this.supportedPHPVersions = [
            "8.1",
            "8.3",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
