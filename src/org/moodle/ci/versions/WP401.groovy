package org.moodle.ci.versions

class WP401 extends Version {
    WP401() {
        this.name = "401"
        this.defaultBranch = "WORKPLACE_401_LATEST"
        this.supportedPHPVersions = [
            "7.4",
            "8.0",
            "8.1"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
