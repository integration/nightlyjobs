package org.moodle.ci.versions

class M310 extends Version {
    M310() {
        this.name = "310"
        this.defaultBranch = "MOODLE_310_STABLE"
        this.supportedPHPVersions = [
            "7.2",
            "7.3",
            "7.4",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
