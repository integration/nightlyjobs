package org.moodle.ci.versions

class WPR403 extends Version {
    WPR403() {
        this.name = "403"
        this.defaultBranch = "WORKPLACE_ROLLING_403_LATEST"
        this.supportedPHPVersions = [
            "8.0",
            "8.2"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
