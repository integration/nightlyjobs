package org.moodle.ci.versions

class WP38 extends Version {
    WP38() {
        this.name = "38"
        this.defaultBranch = "WORKPLACE_38_LATEST"
        this.supportedPHPVersions = [
            "7.1",
            "7.2",
            "7.3",
            "7.4",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
