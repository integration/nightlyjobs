package org.moodle.ci.versions

class WP402 extends Version {
    WP402() {
        this.name = "402"
        this.defaultBranch = "WORKPLACE_402_LATEST"
        this.supportedPHPVersions = [
            "8.0",
            "8.2"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
