package org.moodle.ci.versions

class WPR404 extends Version {
    WPR404() {
        this.name = "404"
        this.defaultBranch = "WORKPLACE_ROLLING_404_LATEST"
        this.supportedPHPVersions = [
            "8.1",
            "8.3"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
