package org.moodle.ci.versions

class WPR402 extends Version {
    WPR402() {
        this.name = "402"
        this.defaultBranch = "WORKPLACE_ROLLING_402_LATEST"
        this.supportedPHPVersions = [
            "8.0",
            "8.2"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
