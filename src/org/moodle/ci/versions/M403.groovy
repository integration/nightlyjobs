package org.moodle.ci.versions

class M403 extends Version {
    M403() {
        this.name = "403"
        this.defaultBranch = "MOODLE_403_STABLE"
        this.supportedPHPVersions = [
            "8.0",
            "8.2",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
