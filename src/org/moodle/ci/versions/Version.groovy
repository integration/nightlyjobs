package org.moodle.ci.versions

class Version {
    protected String name

    protected String defaultBranch

    protected String[] supportedPHPVersions

    protected String[] supportedDatabases

    /**
     * Fetch the name of the default git branch for this Moodle version.
     */
    String getDefaultBranch() {
        return defaultBranch
    }

    /**
     * Fetch the complete list of PHP Versions supported by this version of Moodle.
     */
    String[] getPhpVersions() {
        return supportedPHPVersions.sort()
    }

    /**
     * Fetch the complete list of Database Engines supported by this version of Moodle.
     */
    String[] getDatabases() {
        return supportedDatabases
    }

    /**
     * Fetch the highest supported version of PHP supported by this version of Moodle.
     */
    String getHighestSupportedVersion() {
        def versions = getPhpVersions()

        return versions.sort()[versions.size() - 1]
    }

    /**
     * Fetch the lowest supported version of PHP supported by this version of Moodle.
     */
    String getLowestSupportedVersion() {
        def versions = getPhpVersions()

        return versions.sort()[0]
    }
}
