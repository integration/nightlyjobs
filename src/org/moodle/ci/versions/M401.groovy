package org.moodle.ci.versions

class M401 extends Version {
    M401() {
        this.name = "401"
        this.defaultBranch = "MOODLE_401_STABLE"
        this.supportedPHPVersions = [
            "7.4",
            "8.0",
            "8.1",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
