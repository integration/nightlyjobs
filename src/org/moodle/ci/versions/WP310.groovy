package org.moodle.ci.versions

class WP310 extends Version {
    WP310() {
        this.name = "310"
        this.defaultBranch = "WORKPLACE_310_LATEST"
        this.supportedPHPVersions = [
            "7.2",
            "7.3",
            "7.4",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
