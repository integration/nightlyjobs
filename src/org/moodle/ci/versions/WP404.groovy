package org.moodle.ci.versions

class WP404 extends Version {
    WP404() {
        this.name = "404"
        this.defaultBranch = "WORKPLACE_404_LATEST"
        this.supportedPHPVersions = [
            "8.1",
            "8.3"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
