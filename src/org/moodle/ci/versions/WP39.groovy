package org.moodle.ci.versions

class WP39 extends Version {
    WP39() {
        this.name = "39"
        this.defaultBranch = "WORKPLACE_39_LATEST"
        this.supportedPHPVersions = [
            "7.2",
            "7.3",
            "7.4",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
