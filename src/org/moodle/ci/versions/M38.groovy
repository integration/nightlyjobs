package org.moodle.ci.versions

class M38 extends Version {
    M38() {
        this.name = "38"
        this.defaultBranch = "MOODLE_38_STABLE"
        this.supportedPHPVersions = [
            "7.1",
            "7.2",
            "7.3",
            "7.4",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
