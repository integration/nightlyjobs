package org.moodle.ci.versions

class M36 extends Version {
    M36() {
        this.name = "36"
        this.defaultBranch = "MOODLE_36_STABLE"
        this.supportedPHPVersions = [
            "7.0",
            "7.1",
            "7.2",
            "7.3",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
