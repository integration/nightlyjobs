package org.moodle.ci.versions

class WP400 extends Version {
    WP400() {
        this.name = "400"
        this.defaultBranch = "WORKPLACE_400_LATEST"
        this.supportedPHPVersions = [
            "7.3",
            "8.0",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
