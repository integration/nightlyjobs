package org.moodle.ci.versions

class M39 extends Version {
    M39() {
        this.name = "39"
        this.defaultBranch = "MOODLE_39_STABLE"
        this.supportedPHPVersions = [
            "7.2",
            "7.3",
            "7.4",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
