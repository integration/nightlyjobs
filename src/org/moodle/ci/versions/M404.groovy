package org.moodle.ci.versions

class M404 extends Version {
    M404() {
        this.name = "404"
        this.defaultBranch = "MOODLE_404_STABLE"
        this.supportedPHPVersions = [
            "8.1",
            "8.3",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
