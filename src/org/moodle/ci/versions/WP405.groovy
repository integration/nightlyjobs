package org.moodle.ci.versions

class WP405 extends Version {
    WP405() {
        this.name = "405"
        this.defaultBranch = "WORKPLACE_405_LATEST"
        this.supportedPHPVersions = [
            "8.1",
            "8.3"
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
