package org.moodle.ci.versions

class M35 extends Version {
    M35() {
        this.name = "35"
        this.defaultBranch = "MOODLE_35_STABLE"
        this.supportedPHPVersions = [
            "7.0",
            "7.1",
            "7.2",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
