package org.moodle.ci.versions

class WP311 extends Version {
    WP311() {
        this.name = "311"
        this.defaultBranch = "WORKPLACE_311_LATEST"
        this.supportedPHPVersions = [
            "7.3",
            "8.0",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
