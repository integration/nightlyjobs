package org.moodle.ci.versions

class M311 extends Version {
    M311() {
        this.name = "311"
        this.defaultBranch = "MOODLE_311_STABLE"
        this.supportedPHPVersions = [
            "7.3",
            "8.0",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
