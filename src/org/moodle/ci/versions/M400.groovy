package org.moodle.ci.versions

class M400 extends Version {
    M400() {
        this.name = "400"
        this.defaultBranch = "MOODLE_400_STABLE"
        this.supportedPHPVersions = [
            "7.3",
            "8.0",
        ]
        this.supportedDatabases = [
            "pgsql",
            "mysqli",
            "mariadb",
            "sqlsrv",
            "oci",
        ]
    }
}
