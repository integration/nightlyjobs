package org.moodle.ci

/**
 * A Moodle Continuous Integration Moodle plugin to be installed.
 */
class MoodlePlugin {
    /**
     * The plugin repository URL.
     */
    protected String url

    /**
     * The Moodle folder where the plugin will be installed.
     */
    protected String path

    /**
     * The plugin branch to install.
     */
    protected String branch

    /**
     * The URL of the repository to fetch from.
     *
     * @return String
     */
    String getUrl() {
        if (url) {
          return url
        }

        throw new Exception('No URL repository specified.')
    }

    /**
     * The folder where the plugin will be installed.
     *
     * @return String
     */
    String getPath() {
        if (path) {
          return path
        }

        throw new Exception('No path specified.')
    }

    /**
     * The plugin branch to install.
     */
    String getBranch() {
        if (branch) {
          return branch
        }

        return ''
    }

    /**
     * Get a String in the expected format by the ci-runner.
     *
     * @return  The string format used by the ci-runner.
     */
    String getPluginAsString() {
        if (url && path) {
          return "${url}|${path}|" + getBranch()
        }
        return null
    }
}
