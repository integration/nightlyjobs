package org.moodle.ci.maintenance;

import com.sonyericsson.jenkins.plugins.bfa.model.FailureCauseBuildAction;

/**
 * Task to easily set the description of failed builds in a view to the matching failure causes (indications)
 *
 * Just select the view (usually "All" or "Current Fails"...) and, for all the builds currently failed on it,
 * the script will look for BFA indications for that build, setting the build description with that information.
 */
class SetBuildDescriptionToIndications {

  /**
   * Jenkins view (exact matching) that contains the jobs to be examined.
   */
  protected view = 'All'

  /**
   * Run the configured set description operation.
   */
  int run(Script out) {

    // For all the jobs in the configured view.
    hudson.model.Hudson.instance.getView(view).items.each() {
      // Get the last build
      def build = it.getLastBuild()
      // If they are failed and haven't any description.
      if (build && build.result == Result.FAILURE && !build.description) {
        out.println "Job: " + it.fullDisplayName

        // Look for detected build failure causes (indications).
        def BFA = build.actions.find{ it instanceof FailureCauseBuildAction };
        if (BFA != null) {
          // Join all the indications into string.
          def indicationsList = BFA.getFoundFailureCauses().collect{ it.getName() }
          def indications = indicationsList.join(' + ')

          // Failure causes found, set the description to them.
          if (indications) {
            out.println("  Failure causes: " + indications)
            build.description = indications
            out.println "  Set!"
          }
        }
      }
    }
    return 0
  }
}
