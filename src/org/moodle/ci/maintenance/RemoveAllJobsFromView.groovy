package org.moodle.ci.maintenance;

import org.jenkinsci.plugins.workflow.job.WorkflowJob
import org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition

/**
 * Task to easily remove all the jobs in a view
 */
class RemoveAllJobsFromView {

    /**
     * Configurable name of the target view which jobs will be deleted. Must exist.
     */
    protected String targetViewName = 'B - 403'

    /**
     * Filter (substring matching) to restrict the deletion operation.
     */
    protected filter = 'S.99.99'

    /**
     * Run the configured deletion operation.
     */
    int run(Script out) {
        def targetView = Jenkins.instance.getView(targetViewName)
        if (targetView == null) {
          out.println 'Configuration error: targetViewName view does not exist'
          return 1
        }

    if (targetView.getDisplayName().contains('main')) {
      out.println 'Configuration error: cannot delete jobs from main (B - main) view'
          return 1
        }

        // Iterate over all the projects in the source view.
        for (item in targetView.getItems()) {
          def name = item.getName()
          out.println 'Processing "' + name + '"'

          // Apply filter if existing.
          if (filter != '' && !name.contains(filter)) {
            out.println '  Skipped, does not match "' + filter + "'"
            continue
          }

          // Remove the job.
          item.delete()
          out.println '  Deleted "' + name + '"'
        }
        return 0
    }
}
