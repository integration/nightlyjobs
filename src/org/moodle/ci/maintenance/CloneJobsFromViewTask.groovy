package org.moodle.ci.maintenance;

import org.jenkinsci.plugins.workflow.job.WorkflowJob
import org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition
import hudson.model.ParametersDefinitionProperty

/**
 * Task to easily clone jobs from a view to another
 *
 * Clone jobs from a view/branch to another view/branch,
 * performing all the required modifications.
 *
 * Requires:
 *   - Both source and target views to exist and be configured below.
 *   - New runner, versions... of scripts to be already available.
 * Supports:
 *   - Specify source and target view name
 *   - Filter jobs by regex name matching.
 */
class CloneJobsFromViewTask {

    /**
     * Source view name, from where jobs will be picked. Must exist.
     */
    protected String sourceViewName = 'B - main'

    /**
     * Configurable name of the target view. Must exist.
     */
    protected String targetViewName = 'B - 403'

    /**
     * Prefix to use in the name of the new jobs (should match view name number).
     */
    protected String targetPrefix = '403'

    /**
     * Filter (substring matching) to restrict the clone operation.
     */
    protected filter = 'S.99.99'

    /**
     * Run the configured clone operation.
     */
    int run(Script out) {
        // Verify views exist.
        def sourceView = Jenkins.instance.getView(sourceViewName)
        if (sourceView == null) {
          out.println 'Configuration error: sourceViewName view does not exist'
          return 1
        }
        def targetView = Jenkins.instance.getView(targetViewName)
        if (targetView == null) {
          out.println 'Configuration error: targetViewName view does not exist'
          return 1
        }

        // Iterate over all the projects in the source view.
        for (item in sourceView.getItems()) {
          def name = item.getName()

          // Apply filter if existing.
          if (filter != '' && !name.contains(filter)) {
            continue
          }

          out.println 'Processing "' + name + '"'

          // Calculate new name and verify the job doesn't exist.
          def match = (name =~ /^([A-Z]+)([0-9]*)(\..*)$/)
          def namePrefix = ''
          def nameNumber = ''
          def nameSuffix = ''
          if (match.find()) {
            namePrefix = match.group(1)
            nameNumber = match.group(2)
            nameSuffix = match.group(3)
          } else {
              out.println '  Skipped, unable to split job name into prefix and suffix'
              continue
          }
          def newName = namePrefix + targetPrefix + nameSuffix

          if (Jenkins.instance.getItem(newName)) {
            out.println '  Skipped, already exits "' + newName + '"'
            continue
          }
          out.println '  Creating new job "' + newName + "'"

          // Copy, disable and save.
          def job = Jenkins.instance.copy(item, newName)
          job.disabled = true
          out.println '    Job created'

          // Apply pipeline replacements
          if (job.class.simpleName == 'WorkflowJob') {
            def script = job.getDefinition().getScript()

            // Replace the moodleVersion to the target one.
            def search = ''
            def replacement = ''

            // If the job is a Moodle one (W or S)
            if (namePrefix == 'W' || namePrefix == 'S') {
              if (nameNumber == '') {  // The source is a main job.
                search = 'Main()'
                replacement = 'M' + targetPrefix + '()'
              } else {                 // The source is a numerical branch job
                search = 'M' + nameNumber + '()'
                replacement = 'M' + targetPrefix + '()'
              }

            // If the job is a Workplace one (MW)
            } else if (namePrefix == 'MW') {
              if (nameNumber == '') {  // The source is a main job.
                search = 'Main()'
                replacement = 'WP' + targetPrefix + '()'
              } else {                 // The source is a numerical branch job
                search = 'WP' + nameNumber + '()'
                replacement = 'WP' + targetPrefix + '()'
              }

            // If the job is a Workplace Rolling one (MWR), they use different Versions (WPRxxxx)
            } else if (namePrefix == 'MWR') {
              if (nameNumber == '') {  // The source is a main job.
                search = 'Main()'
                replacement = 'WPR' + targetPrefix + '()'
              } else {                 // The source is a numerical branch job
                search = 'WPR' + nameNumber + '()'
                replacement = 'WPR' + targetPrefix + '()'
              }

            // If the job is a Mobile one (MM, MMP)
            // (note this could go together with W and S jobs, but for future
            // expansion we leave it apart, no matter it's a bit of duped code)
            } else if (namePrefix == 'MM' || namePrefix == 'MMP') {
              if (nameNumber == '') {  // The source is a main job.
                search = 'Main()'
                replacement = 'M' + targetPrefix + '()'
              } else {                 // The source is a numerical branch job
                search = 'M' + nameNumber + '()'
                replacement = 'M' + targetPrefix + '()'
              }
            }

            script = script.replace(search, replacement)
            job.setDefinition(new CpsFlowDefinition(script, true))
            out.println '    Pipeline script adapted: "' + search + '" => "' + replacement + '"'
          }

          // Apply changes to the description if needed.
          def description = job.getDescription()
          if (description && nameNumber) {
              def newDescription = description.replace(nameNumber, targetPrefix)
              if (newDescription != description) {
                  job.setDescription(newDescription)
                  out.println '    Description changed from "' + description + '" to "' + newDescription + '"'
              }
          }

          // Apply parameter replacements.
          job.getProperties().values().each { property ->
            if (property instanceof ParametersDefinitionProperty) {
              property.getParameterDefinitionNames().each { paramName ->
                // Any parameter which name contains "branch" and with default value = "main"
                // is replaced by new MOODLE_XX_STABLE default value
                if (paramName.contains("branch")) {
                  def paramDefaultValue = property.getParameterDefinition(paramName).getDefaultParameterValue().getValue()
                  if (paramDefaultValue == 'main') {
                    def newDefaultValue = 'MOODLE_' + targetPrefix + '_STABLE'
                    property.getParameterDefinition(paramName).setDefaultValue(newDefaultValue)
                    out.println '    Parameter "' + paramName + '" default changed: "main" => "' + newDefaultValue + '"'
                  }
                }
              }
            }
          }

          // Save the job
          job.save()
        }
        return 0
    }
}
