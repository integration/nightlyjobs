package org.moodle.ci.maintenance;

import org.jenkinsci.plugins.workflow.job.WorkflowJob
import org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition
import hudson.model.ParametersDefinitionProperty

/**
 * Task to easily toggle (enable/disable) jobs by name
 *
 * Select the operation to perform (enable/disable) and
 * a name (substring matching) to select the jobs the change
 * will be applied to.
 */
class ToggleJobsByName {

    /**
     * Action to perform (enable, disable).
     */
    protected String action = 'disable'

    /**
     * Filter (substring matching) to restrict the scope of the toggle operation.
     */
    protected filter = 'Rebase security branch'

    /**
     * Run the configured toggle operation.
     */
    int run(Script out) {
        // Validate the action.
        if (action != 'enable' && action != 'disable') {
          out.println 'Configuration error: action only can be "disable" (default) or "enable"'
          return 1
        }

        // Iterate over all the projects in the server.
        for (item in Jenkins.instance.getAllItems(Job.class)) {
          def name = item.getName()

          // Apply filter if existing.
          if (filter != '' && !name.contains(filter)) {
            continue
          }

          out.println 'Processing "' + name + '"'

          if (action == 'enable') {
              if (item.disabled == true) {
                  item.disabled = false
                  out.println '  Job enabled!'
              } else {
                  out.println '  Skipped, the job is already enabled.'
                  continue
              }
          }

          if (action == 'disable') {
              if (item.disabled == false) {
                  item.disabled = true
                  out.println '  Job disabled!'
              } else {
                  out.println '  Skipped, the job is already disabled.'
                  continue
              }
          }

          // Save the job
          item.save()
        }
        return 0
    }
}
