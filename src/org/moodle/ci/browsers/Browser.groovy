package org.moodle.ci.browsers

class Browser {
    protected String name

    protected String defaultTags

    Boolean headless

    Boolean debug

    def getName() {
        return name
    }

    def getDefaultTags() {
        return defaultTags
    }
}
