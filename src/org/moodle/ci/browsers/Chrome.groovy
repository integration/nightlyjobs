package org.moodle.ci.browsers

class Chrome extends Browser {
    Chrome() {
        this.name = "chrome"

        // Do not run non-javascript scenarios. These will be run by BrowserKit.
        // And skip known Chrome problems.
        this.defaultTags = "@javascript&&~@skip_chrome_zerosize&&~@skip_interim"
    }
}
