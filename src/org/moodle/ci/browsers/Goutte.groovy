package org.moodle.ci.browsers

class Goutte extends Browser {
    Goutte() {
        this.name = "goutte"

        // Do not run Javascript scenarios. Goutte does not support them.
        this.defaultTags = "~@javascript&&~@skip_interim"
    }
}
