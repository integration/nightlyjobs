package org.moodle.ci.browsers

class BrowserKit extends Browser {
    BrowserKit() {
        this.name = "browserkit"

        // Do not run Javascript scenarios. BrowserKit does not support them.
        this.defaultTags = "~@javascript&&~@skip_interim"
    }
}

