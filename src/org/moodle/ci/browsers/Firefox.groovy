package org.moodle.ci.browsers

class Firefox extends Browser {
    Firefox() {
        this.name = "firefox"

        // Do not run non-javascript scenarios. These will be run by BrowserKit.
        this.defaultTags = "@javascript&&~@skip_interim"
    }
}
