package org.moodle.ci.stats

import com.cloudbees.groovy.cps.NonCPS

import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.time.TimeCategory

import hudson.model.AbstractItem
import jenkins.metrics.impl.TimeInQueueAction
import jenkins.model.Jenkins

/**
 * Get daily usage statistics to be aggregated elsewhere.
 */
class GetUsageStatistics {

  /**
   * Jobs groups criteria.
   */
  protected HashMap jobGroups = [
    'integration' : '^W[\\.\\d].*',
    'security'    : '^S[\\.\\d].*',
    'performance' : '^P[\\.\\d].*',
    'workplace'   : '^MWR?[\\.\\d].*',
    'mobile'      : '^MMP?[\\.\\d].*',
    'dev'         : '^DEV\\.\\d\\d.*',
    'dev security': '^SDEV\\.\\d\\d.*',
    'tracker'     : '^S?TR .*',
    'github'      : '^GH .*',
    'maintenance' : '^MAINT .*',
    'wip'         : '^WIP\\.\\d\\d.*',
    'other'       : null,
  ]

  /**
   * Jobs types criteria.
   */
  protected HashMap jobTypes = [
    'phpunit' : '.*PHPUnit.*',
    'behat'   : '.*Behat.*',
    'rebase'  : '.*Rebase security.*',
    'tobic'   : '^TR - Bulk pre-launch.*',
    'queues'  : '^TR - Manage queues.*',
    'other'   : null,
  ]

  /**
   * Jobs statuses criteria.
   */
  protected HashMap buildStatuses = [
    'success' : 'SUCCESS',
    'failure' : 'FAILURE',
    'aborted' : 'ABORTED',
    'other'   : null,
  ]

  /**
   * Custom day we want to be processed, it overrrides the default "yesterday" behaviour.
   */
  protected dayToProcess
  /**
   * Start of the interval to gather buids information.
   */
  private Long startPeriod

  /**
   * End of the interval to gather buids information.
   */
  private Long endPeriod

  /**
   * Script / Printer, to get access to out, env and others.
   */
  protected Script out

  /**
   * Workspace directory, available only when passed from the caller.
   */
  protected workspace

  /**
   * Calculate the UTC/GMT/epoch start and end times (ms) od the day (from 00:00:00 to 23:59:59)
   *
   * If a Date is passed then the the times are calculated for that day. If not,
   * then the default behaviour applies and the times are calculated for "yesterday".
   */
  @NonCPS // We need this because sandboxing sometimes makes TimeCategory not to work. See https://issues.jenkins.io/browse/JENKINS-40154
  getStartEndTimesMillis(String dayToProcess) {
    // Let's calculate the interval (start and end) of dates we are going
    // to use to find target jobs (by job start time).
    def processDate = new Date() // So we'll process yesterday by default.
    if (dayToProcess) {
      out.println 'Day to process: ' + dayToProcess
      // We have asked explicitly for a date, that takes precedence over the default.
      // (have to add 1 day. because the calculations below are always for one day before
      // the one decided to be processed).
      processDate = Date.parse('yyyy-MM-dd', dayToProcess) + 1
    }

    // Yesterday's (UTC) 00:00:00
    def Long start = use (TimeCategory) {
      (processDate - 1.days).clearTime().getTime()
    }

    // Yesterday's (UTC) 23:59:00
    def Long end = use (TimeCategory) {
      (processDate.clearTime() - 1.seconds).getTime()
    }

    return [start, end]
  }

  /**
   * Run the script.
   */
  String run(Script out) {
    this.out = out

    // We are going to handle everything with UTC timezone. Always.
    TimeZone.setDefault(TimeZone.getTimeZone('UTC'))

    (startPeriod, endPeriod) = getStartEndTimesMillis(dayToProcess)

    // Defaults to be used by different hash maps.
    HashMap hmGroups = [jobGroups.keySet().toArray(), Collections.nCopies(jobGroups.size(), 0)].transpose().collectEntries()
    HashMap hmTypes = [jobTypes.keySet().toArray(), Collections.nCopies(jobTypes.size(), 0)].transpose().collectEntries()
    HashMap hmStatuses = [buildStatuses.keySet().toArray(), Collections.nCopies(buildStatuses.size(), 0)].transpose().collectEntries()

    // We are going to accumulate a bunch of information, let's init everything with 0s.
    HashMap info = [
      'jobsByGroup' : hmGroups.clone(),
      'jobsByType'  : hmTypes.clone(),

      'buildsByGroup'  : hmGroups.clone(),
      'buildsByType'   : hmTypes.clone(),
      'buildsByStatus' : hmStatuses.clone(),

      'durationByGroup'  : hmGroups.clone(),
      'durationByType'   : hmTypes.clone(),
      'durationByStatus' : hmStatuses.clone(),

      'queuedByGroup'  : hmGroups.clone(),
      'queuedByType'   : hmTypes.clone(),
      'queuedByStatus' : hmStatuses.clone(),
    ]

    out.println 'Processing builds started between ' + this.startPeriod + ' and ' + this.endPeriod
    out.println '(from ' + new Date(this.startPeriod) + ' to ' + new Date(this.endPeriod) + ')'

    // Iterate over all the jobs in the instance.
    Jenkins.instance.getAllItems(AbstractItem.class).each { job ->

      // Calculate the job group only once.
      def jobGroup = 'other'
      for (group in jobGroups) {
        if (group.value && job.getFullName().matches(group.value)) {
          jobGroup = group.key
        }
      }
      info['jobsByGroup'][jobGroup]++
      if (jobGroup == 'other') { // Let's log the missing match.
        out.println 'INFO: Group not found for job (\'other\' used): ' + job.getFullName()
      }

      // Calculate the job type once.
      def jobType = 'other'
      for (type in jobTypes) {
        if (type.value && job.getFullName().toLowerCase().matches(type.value.toLowerCase())) {
          jobType = type.key
        }
      }
      info['jobsByType'][jobType]++
      if (jobType == 'other') { // Let's log the missing match.
        out.println 'INFO: Type not found for job (\'other\' used): ' + job.getFullName()
      }

      // Feed the remaining job stats (that require access to individual builds).
      processJobBuildsStats(job, info, jobGroup, jobType)
    }

    // Ok, we have all the daily metrics calculated, time to process it.

    // Let's build the structure:
    def day = new Date(this.startPeriod).format('yyyy-MM-dd')
    def month = new Date(this.startPeriod).format('yyyy-MM')
    HashMap results = [
      (day) : [
        'from' : this.startPeriod,
        'to'   : this.endPeriod,
        'stats': info,
      ],
    ]

    // If we aren't aware of any workspace, just generate the daily JSON
    // contents print and return them.
    if (!this.workspace) {
      def jsonBuilder = new JsonBuilder(results)
      out.println jsonBuilder.toPrettyString()
      return jsonBuilder.toPrettyString()
    }

    // If we are being executed within a job build, let's look for any previous exisiting
    // JSON file, load it and replace/append this execution results (YYYY-MM-DD is the key).
    def targetFileName = 'GetUsageStatistics-' + month + '.json'

    def HashMap allContents = [:]

    // If the file exists, load it.
    if (out.fileExists(targetFileName)) {
      def jsonSlurper = new JsonSlurper()
      def fileContents = out.readFile(file: targetFileName)
      allContents = jsonSlurper.parseText(fileContents)
    }

    // Replace/add the just processed day in the contents.
    if (allContents.containsKey(day)) {
      allContents.replace(day, results[day])
    } else {
      allContents.put(day, results[day])
    }

    // Rewrite the file with the new contents (sorted by key, aka, date).
    def jsonBuilder = new JsonBuilder(allContents.sort())
    out.writeFile(file: targetFileName, text: jsonBuilder.toPrettyString())

    // We are done, retunr the pretty json, just in case.
    return jsonBuilder.toPrettyString()
  }

  /**
   * Feed job stats (requiring access to the job builds) for a given job.
   */
  int processJobBuildsStats(AbstractItem job, HashMap info, String jobGroup, String jobType) {
    // Let's calculate the builds the job has started over the target period configured.
    def builds = job.getBuilds().byTimestamp(this.startPeriod, this.endPeriod)

    // And iterate over all them, extracting and feed the stats.
    builds.each { build ->

      // Calculate the build status.
      def buildStatus = 'other'
      for (status in buildStatuses) {
        if (status.value && build.getResult().toString().matches(status.value)) {
          buildStatus = status.key
        }
      }
      info['buildsByStatus'][buildStatus]++
      if (buildStatus == 'other') { // Let's log the missing match.
        out.println 'INFO: Status not found for job (\'other\' used): ' + job.getFullName() + ' #' + build.number + ': ' + build.getResult()
      }

      // Add build counters.
      info['buildsByGroup'][jobGroup]++
      info['buildsByType'][jobType]++

      // We need access to the metrics plugin to get split duration and queued information.
      // (will fallback to simple duration only if the plugin information is not available).
      def durationMillis = 0
      def queuedMillis = 0
      def TimeInQueueAction action = build.actions.find{ it instanceof TimeInQueueAction }
      if (action) {
        durationMillis = action.getExecutingTimeMillis()
        queuedMillis = action.getBlockedTimeMillis()
      } else { // Pity, we don't have access to detailed metrics, use total build time.
        durationMillis = build.getDuration()
        queuedMillis = 0
      }

      // And the durations.
      info['durationByGroup'][jobGroup] += durationMillis
      info['durationByType'][jobType] += durationMillis
      info['durationByStatus'][buildStatus] += durationMillis

      // And the queued time.
      info['queuedByGroup'][jobGroup] += queuedMillis
      info['queuedByType'][jobType] += queuedMillis
      info['queuedByStatus'][buildStatus] += queuedMillis
    }

    return 0
  }
}

// To be used while testing within the script console.
// (commented for normal execution as shared library).
//def gus = new GetUsageStatistics()
//gus.run(this)
