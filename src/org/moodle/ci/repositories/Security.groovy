package org.moodle.ci.repositories

class Security extends Repository {
    Security() {
        this.url = "git@git.in.moodle.com:integration/security.git"
        this.credentialsId = 'security'
    }
}
