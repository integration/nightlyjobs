package org.moodle.ci.repositories

class Integration extends Repository {
    Integration() {
        this.url = "https://git.in.moodle.com/moodle/integration.git"
    }
}
