package org.moodle.ci.repositories

class Workplace extends Repository {
    Workplace() {
        this.url = "git@git.in.moodle.com:workplace/workplacehq.git"
        this.credentialsId = 'workplace'
    }
}
