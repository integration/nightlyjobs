package org.moodle.ci.repositories

class Repository {
    protected String url

    protected String credentialsId

    def getUrl() {
        return url
    }

    def getCredentials() {
        return credentialsId
    }
}
