/**
 * Licensed under the Apache License, Version 2.0 (the "License")
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.moodle.ci

import com.tikal.hudson.plugins.notification.*
import com.tikal.hudson.plugins.notification.model.*
import hudson.model.*
import org.jenkinsci.plugins.workflow.support.steps.build.RunWrapper
import java.util.logging.Logger;

class Notifier {
    public static void notify(Task task, RunWrapper run) {
        LOGGER.info("Notifying of task completion")
        if (!task.shouldNotify()) {
            LOGGER.info("Task requested not to send")
        }

        String expandedUrl = task.getNotify()
        String urlIdString = String.format("url '%s'", expandedUrl)

        int triesRemaining = 3
        boolean failed = false
        while({
            try {
                // Expand out the URL from environment + url.
                LOGGER.info(String.format("Notifying endpoint with %s",
                        urlIdString))
                JobState jobState = buildJobState(run)
                Protocol.HTTP.send(expandedUrl,
                                   Format.JSON.serialize(jobState),
                                   10000,
                                   true)
            } catch (Throwable error) {
                failed = true
                LOGGER.warning(String.format("Failed to notify endpoint with %s - %s: %s",
                        urlIdString, error.getClass().getName(), error.getMessage()))
                if (triesRemaining > 0) {
                    LOGGER.warning(String.format("Reattempting to notify endpoint with %s (%d tries remaining)",
                            urlIdString, triesRemaining))
                }
            }
            (failed && --triesRemaining >= 0)
        }());
    }

    private static JobState buildJobState(RunWrapper run)
        throws IOException, InterruptedException
    {

        BuildState buildState = new BuildState()
        buildState.setNumber(run.getNumber())
        buildState.setFullUrl(run.getAbsoluteUrl())
        buildState.setStatus(run.getCurrentResult())
        buildState.setTimestamp(run.getTimeInMillis())

        // These values are now available from the RunWrapper. Fetch them
        // from the raw Run object.
        hudson.model.Run thisRun = run.getRawBuild()
        buildState.setUrl(thisRun.getUrl())
        buildState.setQueueId(thisRun.getQueueId())
        buildState.setPhase(Phase.COMPLETED)
        buildState.setScm(new ScmState())

        StringBuilder log = new StringBuilder("")
        List<String> logEntries = run.getRawBuild().getLog(10)
        for (String entry: logEntries) {
            log.append(entry)
            log.append("\n")
        }
        buildState.setLog(log)

        JobState jobState = new JobState()
        jobState.setBuild(buildState)

        org.jenkinsci.plugins.workflow.job.WorkflowJob thisJob = thisRun.project

        jobState.setName(thisJob.getName())
        jobState.setDisplayName(thisJob.getDisplayName())
        jobState.setUrl(thisJob.getUrl())

        return jobState
    }

    private static final Logger LOGGER = Logger.getLogger(Notifier.class.getName());
}
