package org.moodle.ci.failures;

import com.sonyericsson.jenkins.plugins.bfa.model.FailureCauseBuildAction;

import groovy.json.JsonBuilder

import hudson.model.AbstractItem
import hudson.model.Result
import hudson.tasks.junit.TestResultAction
import jenkins.model.Jenkins

/**
 * Task to gather information about all the failed tests in recent builds.
 *
 * This task iterates over all the recent (decided by number and age) builds of various jobs
 * (decided by regular expression), looking for failed tests, their potential causes and
 * some extra information about them.
 *
 * Results are returned in JSON format for further processing elsewhere.
 */
class GetFailuresInformation {

  protected JOBFILTER = '^(W[\\.\\d]|S[\\.\\d]).*' // W and S jobs only, aka upstream ones.
  protected NUMBUILDS = 20 // Max number of builds to look for information on every job.
  protected NUMDAYS = 7 // Max number of days to look backwards, older builds will be discarded.

  /**
   * Run the script.
   */
  String run(Script out) {

    def results = [] // Where all the information will be accumulated.

    // Get all the jobs in the instance.
    def jobs = Jenkins.instance.getAllItems(AbstractItem.class)

    for (job in jobs) {
      // Filter out jobs we aren't interested on.
      if (!job.fullName.matches(JOBFILTER)) {
        continue
      }
      out.println job.fullName

      // Get last NUMBUILDS (any status) builds in the job. Only if there are builds,
      // and they are better or equal than the "worst", aka, aborted. Note that the results
      // come sorted from newest down to older.
      def builds = job.getLastBuild() ? job.getLastBuildsOverThreshold(NUMBUILDS, Result.ABORTED) : []

      // Let's look for the junit results, failure causes and details of every job.
      for (build in builds) {
        // We are going to gather a lot of information.
        def jobName = job.fullName
        def buildNumber
        def buildUrl
        def buildStartTime
        def buildDuration
        // Failures.
        def failedTests = []
        // Issues.
        def probableIssueManual
        // Details.
        def buildType
        def buildCommit
        def buildPHP
        def buildBranch
        def buildDatabase
        def buildBrowser
        def buildHeadless
        def buildChromeArgs
        def buildFirefoxArgs
        def buildTheme

        // Only look within the NUMDAYS runs. Discard older.
        if (build.getTime() < (new Date() - NUMDAYS)) {
          continue
        }

        // Get the junit results.
        def junitResults = build.actions.find{ it instanceof TestResultAction }
        def failCount = junitResults?.getFailCount()
        // No failures registered, we aren't interested in the build.
        if (!failCount) {
            continue
        }
        def failDetails = junitResults?.getFailedTests()
        // Ensure we also have details, not interested if not.
        if (!failDetails) {
            continue
        }
        buildNumber = build.number
        buildUrl = build.url
        buildStartTime = build.startTimeInMillis
        buildDuration = build.duration
        out.println "  Build number: " + build.number + " (start: " + build.startTimeInMillis + ", duration: " + build.duration + ")"

        // Get the complete logs of the build.
        def buildLog = build.logFile.text

        // Let's extract the the section we are interested on ("Job summary").
        def summaryLog = ""
        def summaryMatcher = (buildLog =~ /(?ms)^= Job summary <<<\n^=+\n((^== ([^\n]*):([^\n]*)\n)+)/)

        if (summaryMatcher.find()) {
          summaryLog = summaryMatcher.group(1)
        } else {
          out.println "    GetFailuresInformation: Job summary not found in logs!"
          continue
        }

        def reader = new BufferedReader(new StringReader(summaryLog))

        def line
        while ((line = reader.readLine()) != null) {
          def matcher = (line =~ /^== (.*): (.*)$/)
          if (matcher.find()) {
            switch (matcher.group(1)) {
              case 'GIT commit':
                buildCommit = matcher.group(2).substring(0,16)
                break
              case 'PHP version':
                buildPHP = matcher.group(2)
                break
              case 'Moodle branch (version.php)':
                buildBranch = matcher.group(2)
                break
              case 'DBTYPE':
                buildDatabase = matcher.group(2)
                break
              case 'JOBTYPE':
                buildType = matcher.group(2)
                break
              case 'BROWSER':
                if (buildType == 'behat') {
                  buildBrowser = matcher.group(2)
                }
                break
              case 'BROWSER_HEADLESS':
                if (buildType == 'behat') {
                  buildHeadless = (matcher.group(2) == '1') ? 'headless' : 'headed'
                }
                break
              case 'BROWSER_CHROME_ARGS':
                buildChromeArgs = matcher.group(2)
                break
              case 'BROWSER_FIREFOX_ARGS':
                buildFirefoxArgs = matcher.group(2)
                break
              case 'BEHAT_SUITE':
                if (buildType == 'behat') {
                  buildTheme = matcher.group(2) ?: 'boost'
                }
                break
            }
          }
        }

        // Build the details map.
        def details = [
          commit: buildCommit,
          php: buildPHP,
          branch: buildBranch,
          database: buildDatabase,
          jobtype: buildType,
          browser: buildBrowser,
          headless: buildHeadless,
          chromeArgs: buildChromeArgs,
          firefoxArgs: buildFirefoxArgs,
          theme: buildTheme,
        ]
        out.println "    Details: " + details.values().findAll({it != null}).join(', ')

        // Look if the build has any manually introduced issue that may be the cause. Only issue code allowed, no extras.
        def buildMatcher = (build.getDescription() =~ /^(MDL-\d+)$/)
        if (buildMatcher.find()) {
          probableIssueManual = buildMatcher.group(1)
          out.println "    Manual issue (probably): " + probableIssueManual
        }

        // Get the detected failure causes (indications).
        def causesList = []
        def bfa = build.actions.find{ it instanceof FailureCauseBuildAction }
        if (bfa != null) {
          // Only consider indications that have MDL-xxx, skipping any FIXED one.
          causesList = bfa.getFoundFailureCauses().findAll {
            it.getName().matches('^ *MDL-\\d+.*')
            ! it.getName().matches('^ *FIXED.*')
          }
        }

        // Let's get all the failed tests.
        for (failed in failDetails) {
          def grandpa = (failed.getParent()?.getParent()?.getName() ?: "").replace("(root)", "")
          def parent = (grandpa ? grandpa + "\\" : "") +  failed.getParent()?.getName()
          def child = failed.getName()
          def probableIssueCause
          out.println "    Test: " + parent + " > " + child

          // Look if the failure matches some of the indication (parent + child basic matching against name or description).
          if (causesList) {
            for (cause in causesList) {
              if (cause.getName().contains(child) || cause.getDescription().contains(child)) {
                def causesMatcher = (cause.getName() =~ /^(MDL-\d+)/)
                if (causesMatcher.find()) {
                  probableIssueCause = causesMatcher.group(1)
                  out.println "      Cause issue (probably): " + probableIssueCause
                  break
                }
              }
            }
          }
          def test = [
            name: child,
            parent: parent,
            probableIssue: probableIssueCause,
          ]
          failedTests.add(test);
        }

        // Build the run map with all the information gathered.
        def buildInfo = [
          job: jobName,
          build: buildNumber,
          url: buildUrl,
          start: buildStartTime,
          duration: buildDuration,
          details: details,
          probableIssue: probableIssueManual,
          tests: failedTests,
        ]

        // Add this build information to the results.
        results.add(buildInfo)
      }
    }

    // We are done, return the whole results as JSON.
    def jsonBuilder = new JsonBuilder(results)
    return jsonBuilder.toPrettyString()
  }
}
