package org.moodle.ci;

import spock.lang.Specification

class MoodlePluginSpec extends Specification {
    def "getUrl() with no url throws exception"() {
        given:
        def url = null
        def path = 'local/codechecker'
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getUrl()

        then:
        def e = thrown(Exception)
        e.message == 'No URL repository specified.'
    }

    def "getUrl() returns specified plugin url"() {
        given:
        def url = 'https://github.com/moodlehq/moodle-local_codechecker'
        def path = 'local/codechecker'
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getUrl()

        then:
        assert output == 'https://github.com/moodlehq/moodle-local_codechecker'
    }

    def "getPath() with no path throws exception"() {
        given:
        def url = 'git://github.com/moodlehq/moodle-local_codechecker'
        def path = null
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getPath()

        then:
        def e = thrown(Exception)
        e.message == 'No path specified.'
    }

    def "getPath() returns specified plugin path"() {
        given:
        def url = 'git://github.com/moodlehq/moodle-local_codechecker'
        def path = 'local/codechecker'
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getPath()

        then:
        assert output == 'local/codechecker'
    }

    def "getBranch() with no branch"() {
        given:
        def url = 'git://github.com/moodlehq/moodle-local_codechecker'
        def path = 'local/codechecker'
        def branch = null

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getBranch()

        then:
        assert output == ''
    }

    def "getBranch() with branch"() {
        given:
        def url = 'git://github.com/moodlehq/moodle-local_codechecker'
        def path = 'local/codechecker'
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getBranch()

        then:
        assert output == 'main'
    }

    def "getPluginAsString() with no branch"() {
        given:
        def url = 'git://github.com/moodlehq/moodle-local_codechecker'
        def path = 'local/codechecker'
        def branch = null

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getPluginAsString()

        then:
        assert output == 'git://github.com/moodlehq/moodle-local_codechecker|local/codechecker|'
    }

    def "getPluginAsString() with branch"() {
        given:
        def url = 'git://github.com/moodlehq/moodle-local_codechecker'
        def path = 'local/codechecker'
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getPluginAsString()

        then:
        assert output == 'git://github.com/moodlehq/moodle-local_codechecker|local/codechecker|main'
    }

    def "getPluginAsString with no url returns null"() {
        given:
        def url = null
        def path = 'local/codechecker'
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getPluginAsString()

        then:
        assert output == null
    }

    def "getPluginAsString with no path returns null"() {
        given:
        def url = 'git://github.com/moodlehq/moodle-local_codechecker'
        def path = null
        def branch = 'main'

        when:
        def plugin = new MoodlePlugin(url: url, path: path, branch: branch)
        def output = plugin.getPluginAsString()

        then:
        assert output == null
    }
}
