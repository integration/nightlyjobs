package org.moodle.ci

import org.moodle.ci.browsers.*
import org.moodle.ci.repositories.*
import org.moodle.ci.versions.*
import spock.lang.Specification

class TaskSpec extends Specification {
    def "getBuildDescription with basics"() {
        given:
        def moodleVersion = new M34()
        def branch = 'MDL-12345-34'

        when:
        def task = new Task(moodleVersion: moodleVersion, branch: branch)
        def output = task.buildDescription

        then:
        assert output == "MDL-12345-34: phpunit pgsql (PHP7.2)"
    }

    def "getBuildDescription with behat"() {
        given:
        def moodleVersion = new M34()
        def branch = 'MDL-12345-34'

        when:
        def task = new Task(task: "behat", moodleVersion: moodleVersion, branch: branch)
        def output = task.buildDescription

        then:
        assert output == "MDL-12345-34: behat pgsql (PHP7.2)"
    }

    def "getBuildDescription with mariadb"() {
        given:
        def moodleVersion = new M34()
        def branch = 'MDL-12345-34'

        when:
        def task = new Task(database: "mariadb", moodleVersion: moodleVersion, branch: branch)
        def output = task.buildDescription

        then:
        assert output == "MDL-12345-34: phpunit mariadb (PHP7.2)"
    }

    def "getBuildDescription with LOWEST PHP version"() {
        given:
        def moodleVersion = new M34()
        def branch = 'MDL-12345-34'

        when:
        def task = new Task(phpVersion: Task.LOWEST, moodleVersion: moodleVersion, branch: branch)
        def output = task.buildDescription

        then:
        assert output == "MDL-12345-34: phpunit pgsql (PHP7.0)"
    }

    def "getBuildDescription with databaseTag"() {
        given:
        def moodleVersion = new M400()
        def branch = 'MDL-12345-34'
        def database = 'someDb'
        def databaseTag = 'someTag'

        when:
        def task = new Task(moodleVersion: moodleVersion, branch: branch, database: database, databaseTag: databaseTag)
        def output = task.buildDescription

        then:
        assert output == "MDL-12345-34: phpunit someDb:someTag (PHP8.0)"
    }

    def "getBuildDescription with app"() {
        given:
        def taskType = 'type'
        def moodleVersion = new M402()
        def branch = 'branch'
        def database = 'database'
        def mobileVersion = 'latest-test'

        when:
        def task = new Task(task: taskType, moodleVersion: moodleVersion, branch: branch,
            database: database, mobileVersion: mobileVersion)
        def output = task.buildDescription

        then:
        assert output == "branch: type database (PHP8.2) - app"
    }

    def "getBuildDescription with bisect session"() {
        given:
        def taskType = 'type'
        def moodleVersion = new M402()
        def branch = 'branch'
        def database = 'database'
        def goodCommit = 'goodCommit'
        def badCommit = 'badCommit'

        when:
        def task = new Task(task: taskType, moodleVersion: moodleVersion, branch: branch,
                database: database, goodCommit: goodCommit, badCommit: badCommit)
        def output = task.buildDescription

        then:
        assert output == "branch: type database (PHP8.2) - bisect"
    }

    def "getBuildDescription with app and bisect session"() {
        given:
        def taskType = 'type'
        def moodleVersion = new M402()
        def branch = 'branch'
        def database = 'database'
        def mobileVersion = 'latest-test'
        def goodCommit = 'goodCommit'
        def badCommit = 'badCommit'

        when:
        def task = new Task(task: taskType, moodleVersion: moodleVersion, branch: branch,
                database: database, mobileVersion: mobileVersion, goodCommit: goodCommit, badCommit: badCommit)
        def output = task.buildDescription

        then:
        assert output == "branch: type database (PHP8.2) - app - bisect"
    }


    def "getBehatParallel() with no behatParallel"() {
        given:

        when:
        def task = new Task()
        def returns = task.getBehatParallel()

        then:
        assert returns == 3
    }

    def "getBehatParallel() with behatParallel"() {
        given:
        def behatParallel = 5

        when:
        def task = new Task(behatParallel : behatParallel)
        def returns = task.getBehatParallel()

        then:
        assert returns == 5
    }

    def "getBehatParallel() with zero behatParallel"() {
        given:
        def behatParallel = 0

        when:
        def task = new Task(behatParallel : behatParallel)
        def returns = task.getBehatParallel()

        then:
        assert returns == 1
    }

    def "getBehatParallel() with low behatParallel"() {
        given:
        def behatParallel = -1

        when:
        def task = new Task(behatParallel : behatParallel)
        def returns = task.getBehatParallel()

        then:
        assert returns == 1
    }

    def "getBehatParallel() with high behatParallel"() {
        given:
        def behatParallel = 999

        when:
        def task = new Task(behatParallel : behatParallel)
        def returns = task.getBehatParallel()

        then:
        assert returns == 10
    }

    def "getBehatReruns() with no behatReruns"() {
        given:

        when:
        def task = new Task()
        def returns = task.getBehatReruns()

        then:
        assert returns == 1
    }

    def "getBehatReruns() with behatReruns"() {
        given:
        def behatReruns = 3

        when:
        def task = new Task(behatReruns : behatReruns)
        def returns = task.getBehatReruns()

        then:
        assert returns == 3
    }

    def "getBehatReruns() with zero behatReruns"() {
        given:
        def behatReruns = 0

        when:
        def task = new Task(behatReruns : behatReruns)
        def returns = task.getBehatReruns()

        then:
        assert returns == 0
    }

    def "getBehatReruns() with low behatReruns"() {
        given:
        def behatReruns = -1

        when:
        def task = new Task(behatReruns : behatReruns)
        def returns = task.getBehatReruns()

        then:
        assert returns == 1
    }

    def "getBehatReruns() with high behatReruns"() {
        given:
        def behatReruns = 999

        when:
        def task = new Task(behatReruns : behatReruns)
        def returns = task.getBehatReruns()

        then:
        assert returns == 9
    }

    def "getGoodCommit() with null goodCommit"() {
        given:
        def goodCommit = null

        when:
        def task = new Task(goodCommit : goodCommit)
        def returns = task.getGoodCommit()

        then:
        assert returns == ""
    }

    def "getGoodCommit() with no goodCommit"() {
        given:

        when:
        def task = new Task()
        def returns = task.getGoodCommit()

        then:
        assert returns == ""
    }

    def "getGoodCommit() with goodCommit"() {
        given:
        def goodCommit = "test_commit"

        when:
        def task = new Task(goodCommit : goodCommit)
        def returns = task.getGoodCommit()

        then:
        assert returns == "test_commit"
    }

    def "getGoodCommit() with empty goodCommit"() {
        given:
        def goodCommit = ""

        when:
        def task = new Task(goodCommit : goodCommit)
        def returns = task.getGoodCommit()

        then:
        assert returns == ""
    }

    def "getGoodCommit() with null badCommit"() {
        given:
        def badCommit = null

        when:
        def task = new Task(badCommit : badCommit)
        def returns = task.getBadCommit()

        then:
        assert returns == ""
    }

    def "getBadCommit() with no badCommit"() {
        given:

        when:
        def task = new Task()
        def returns = task.getBadCommit()

        then:
        assert returns == ""
    }

    def "getBadCommit() with badCommit"() {
        given:
        def badCommit = "test_commit"

        when:
        def task = new Task(badCommit : badCommit)
        def returns = task.getBadCommit()

        then:
        assert returns == "test_commit"
    }

    def "getBadCommit() with empty badCommit"() {
        given:
        def badCommit = ""

        when:
        def task = new Task(badCommit : badCommit)
        def returns = task.getBadCommit()

        then:
        assert returns == ""
    }

    def "getNodeLabel() with no nodeLabel"() {
        given:

        when:
        def task = new Task()
        def returns = task.getNodeLabel()

        then:
        assert returns == "docker"
    }

    def "getNodeLabel() with empty label"() {
        given:
        def nodeLabel = ""

        when:
        def task = new Task(nodeLabel: nodeLabel)
        def returns = task.getNodeLabel()

        then:
        assert returns == "docker"
    }

    def "getNodeLabel() with defined label"() {
        given:
        def nodeLabel = "test_label"

        when:
        def task = new Task(nodeLabel: nodeLabel)
        def returns = task.getNodeLabel()

        then:
        assert returns == "test_label"
    }

    def "getBehatTimeoutMultiplier() with no behatTimeoutMultiplier"() {
        given:

        when:
        def task = new Task()
        def returns = task.getBehatTimeoutMultiplier()

        then:
        assert returns == 0
    }

    def "getBehatTimeoutMultiplier() with correct behatTimeoutMultiplier"() {
        given:
        def behatTimeoutMultiplier = 3

        when:
        def task = new Task(behatTimeoutMultiplier : behatTimeoutMultiplier)
        def returns = task.getBehatTimeoutMultiplier()

        then:
        assert returns == 3
    }

    def "getBehatTimeoutMultiplier() with zero behatTimeoutMultiplier"() {
        given:
        def behatTimeoutMultiplier = 0

        when:
        def task = new Task(behatTimeoutMultiplier : behatTimeoutMultiplier)
        def returns = task.getBehatTimeoutMultiplier()

        then:
        assert returns == 0
    }

    def "getBehatTimeoutMultiplier() with one behatTimeoutMultiplier"() {
        given:
        def behatTimeoutMultiplier = 1

        when:
        def task = new Task(behatTimeoutMultiplier : behatTimeoutMultiplier)
        def returns = task.getBehatTimeoutMultiplier()

        then:
        assert returns == 0
    }

    def "getBehatTimeoutMultiplier() with negative behatTimeoutMultiplier"() {
        given:
        def behatTimeoutMultiplier = -3

        when:
        def task = new Task(behatTimeoutMultiplier : behatTimeoutMultiplier)
        def returns = task.getBehatTimeoutMultiplier()

        then:
        assert returns == 0
    }

    def "getMobileVersion() with no version or app build"() {
        given:
        def mobileVersion = null
        def mobileAppBuildBranch = ''
        def env = [ : ]

        when:
        def task = new Task(mobileVersion: mobileVersion, mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileVersion(env)

        then:
        assert output == ''
    }

    def "getMobileVersion() mobileVersion has precedence over mobileAppBuildBranch"() {
        given:
        def mobileVersion = 'test_version'
        def mobileAppBuildBranch = 'test_build_branch'
        def env = [ : ]

        when:
        def task = new Task(mobileVersion: mobileVersion, mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileVersion(env)

        then:
        assert output == 'test_version'
    }

    def "getMobileVersion() with mobileAppBuildBranch returns info using BUILD_ID env var"() {
        given:
        def mobileVersion = null
        def mobileAppBuildBranch = 'anything'
        def env = [BUILD_ID : 'test_build_id' ]

        when:
        def task = new Task(mobileVersion: mobileVersion, mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileVersion(env)

        then:
        assert output == 'integration_test_build_id'
    }

    def "addMoodlePlugin() properly adds plugins"() {
        given:
        def plugin1 = new MoodlePlugin(
                        url: 'https://url',
                        path: 'local/plugin1',
                        branch: 'main')
        def plugin2 = new MoodlePlugin(
                        url: 'https://url2',
                        path: 'local/plugin2',
                        branch: 'main')

        when:
        def task = new Task([moodlePlugins: [plugin1]])
        task.addMoodlePlugin(plugin2)
        def output = task.getPluginsToInstall()

        then:
        assert output == 'https://url2|local/plugin2|main;https://url|local/plugin1|main'
    }

    def "getPluginsToInstall() with empty value"() {
        given:
        def moodlePlugins = null

        when:
        def task = new Task(moodlePlugins: moodlePlugins)
        def output = task.getPluginsToInstall()

        then:
        assert output == ''
    }

    def "getPluginsToInstall() with one plugin"() {
        given:
        def moodlePlugins = [
            new MoodlePlugin(
                url: 'https://github.com/moodlehq/moodle-local_codechecker',
                path: 'local/plugin1',
                branch: 'master'
            )
        ]

        when:
        def task = new Task(moodlePlugins: moodlePlugins)
        def output = task.getPluginsToInstall()

        then:
        assert output == 'https://github.com/moodlehq/moodle-local_codechecker|local/plugin1|master'
    }

    def "getPluginsToInstall() with more than one plugin"() {
        given:
        def moodlePlugins = [
            new MoodlePlugin(
                url: 'https://github.com/moodlehq/moodle-local_codechecker',
                path: 'local/plugin1',
                branch: 'master'
            ),
            new MoodlePlugin(
                url: 'https://github.com/moodlehq/moodle-local_moodlemobile',
                path: 'local/plugin2'
            ),
        ]

        when:
        def task = new Task(moodlePlugins: moodlePlugins)
        def output = task.getPluginsToInstall()

        then:
        assert output == 'https://github.com/moodlehq/moodle-local_moodlemobile|local/plugin2|;https://github.com/moodlehq/moodle-local_codechecker|local/plugin1|master'
    }

    def "addMoodleConfig() stores all the pairs passed properly"() {
        given:
        def expectation = ["name1": "value 1", "sub/name2": "sub/value 2"]

        when:
        def task = new Task()
        expectation.each{entry ->
            task.addMoodleConfig(entry.key, entry.value)
        }

        then:
        expectation == task.moodleConfig
    }

    def "addMoodleConfigFromJSON() adds the pairs from the JSON string ok"() {
        given:
        def json = '{"name1": "value 1", "sub/name2": "sub/value 2"}'
        def expectation = ["name1": "value 1", "sub/name2": "sub/value 2"]

        when:
        def task = new Task()
        task.addMoodleConfigFromJSON(json)

        then:
        expectation == task.moodleConfig
    }

    def "addMoodleConfigFromJSON() adds recursive stuff from the JSON string ok"() {
        given:
        def json = '{"name1": "value 1", "name2": {"name21": "value 21", "name22": "value 22"}}'
        def expectation = ["name1": "value 1", "name2": ["name21": "value 21", "name22": "value 22"]]

        when:
        def task = new Task()
        task.addMoodleConfigFromJSON(json)

        then:
        expectation == task.moodleConfig
        // Verify that we aren't getting any LazyMap because they
        // are not serializable and Jenkins requires that. As far
        // as we are using the classic JSON parser, it shouldn't
        // be a problem, but let's be sure.
        task.moodleConfig instanceof HashMap
        task.moodleConfig['name2'] instanceof HashMap
    }

    def "addMoodleConfigFromJSON() adds nothing if the JSON string is empty"() {
        given:
        def json = ''
        def expectation = [:]

        when:
        def task = new Task()
        task.addMoodleConfigFromJSON(json)

        then:
        expectation == task.moodleConfig
    }

    def "addMoodleConfigFromJSON() adds nothing if the JSON string is not valid"() {
        given:
        def json = 'invalid'
        def expectation = [:]

        when:
        def task = new Task()
        task.addMoodleConfigFromJSON(json)

        then:
        expectation == task.moodleConfig
    }

    def "addMoodleConfigFromJSON() adds new config pairs, keeping existing ones"() {
        given:
        def json = '{"name1": "value 1", "sub/name2": "sub/value 2"}'
        def expectation = ["name1": "value 1", "sub/name2": "sub/value 2", "name3": "value 3"]

        when:
        def task = new Task(moodleConfig: ["name3": "value 3"])
        task.addMoodleConfigFromJSON(json)

        then:
        expectation == task.moodleConfig
    }

    def "getMoodleConfigJSON() without any config setting"() {
        given:

        when:
        def task = new Task()
        def output = task.getMoodleConfigJSON()

        then:
        assert output == ''
    }

    def "getMoodleConfigJSON() with empty map"() {
        given:

        when:
        def task = new Task(moodleConfig: [:])
        def output = task.getMoodleConfigJSON()

        then:
        assert output == ''
    }

    def "getMoodleConfigJSON() with some config settings"() {
        given:
        def settings = ["name1": "value 1", "sub/name2": "sub/value 2"]

        when:
        def task = new Task(moodleConfig: settings)
        def output = task.getMoodleConfigJSON()

        then:
        assert output ==~ /(?s)^\{.*\}$/
        assert output =~ /"name1": "value 1"/
        assert output =~ /"sub\/name2": "sub\/value 2"/
    }

    def "getPhpunitTestsuite() with no phpunitTestsuite"() {
        given:

        when:
        def task = new Task()
        def output = task.getPhpunitTestsuite()

        then:
        assert output == ''
    }

    def "getPhpunitTestsuite() with null phpunitTestsuite"() {
        given:
        def phpunitTestsuite = null

        when:
        def task = new Task(phpunitTestsuite: phpunitTestsuite)
        def output = task.getPhpunitTestsuite()

        then:
        assert output == ''
    }

    def "getPhpunitTestsuite() with phpunitTestsuite"() {
        given:
        def phpunitTestsuite = 'test_suite'

        when:
        def task = new Task(phpunitTestsuite: phpunitTestsuite)
        def output = task.getPhpunitTestsuite()

        then:
        assert output == 'test_suite'
    }

    def "getPhpunitFilter() with no phpunitFilter"() {
        given:

        when:
        def task = new Task()
        def output = task.getPhpunitFilter()

        then:
        assert output == ''
    }

    def "getPhpunitFilter() with null phpunitFilter"() {
        given:
        def phpunitFilter = null

        when:
        def task = new Task(phpunitFilter: phpunitFilter)
        def output = task.getPhpunitFilter()

        then:
        assert output == ''
    }

    def "getPhpunitFilter() with phpunitFilter"() {
        given:
        def phpunitFilter = 'test_filter'

        when:
        def task = new Task(phpunitFilter: phpunitFilter)
        def output = task.getPhpunitFilter()

        then:
        assert output == 'test_filter'
    }

    def "getBehatSuiteName() with no behatSuite"() {
        given:
        def behatSuite = null

        when:
        def task = new Task(behatSuite: behatSuite)
        def output = task.getBehatSuiteName()

        then:
        assert output == ''
    }

    def "getBehatSuiteName() with no behatSuite named default"() {
        given:
        def behatSuite = null

        when:
        def task = new Task(behatSuite: behatSuite)
        def output = task.getBehatSuiteName("default")

        then:
        assert output == 'default'
    }

    def "getBehatSuiteName() with classic behatSuite"() {
        given:
        def behatSuite = 'classic'

        when:
        def task = new Task(behatSuite: behatSuite)
        def output = task.getBehatSuiteName()

        then:
        assert output == 'classic'
    }

    def "getBehatSuiteName() with classic behatSuite named default (still classic)"() {
        given:
        def behatSuite = 'classic'

        when:
        def task = new Task(behatSuite: behatSuite)
        def output = task.getBehatSuiteName("default")

        then:
        assert output == 'classic'
    }

    def "getBehatTags() without behatTags and browser returns empty"() {
        when:
        def task = new Task()
        def output = task.getBehatTags()

        then:
        assert output == ''
    }

    def "getBehatTags() with behatTags specified get precedence over browser"() {
        given:
        def behatTags = 'testTags'
        def browser = new Chrome()

        when:
        def task = new Task(behatTags: behatTags, browser: browser)
        def output = task.getBehatTags()

        then:
        assert output == 'testTags'
    }

    def "getBehatTags() with browser specified returns its tags"() {
        given:
        def behatTags = null
        def browser = new Chrome()

        when:
        def task = new Task(behatTags: behatTags, browser: browser)
        def output = task.getBehatTags()

        then:
        assert output ==~ /^@javascript.*$/
    }

    def "getBehatName() without behatName returns empty"() {
        when:
        def task = new Task()
        def output = task.getBehatName()

        then:
        assert output == ''
    }

    def "getBehatName() with behatName returns it"() {
        given:
        def behatName = 'testName'

        when:
        def task = new Task(behatName: behatName)
        def output = task.getBehatName()

        then:
        assert output == 'testName'
    }

    def "getNotify() is set by default to a URL"() {
        when:
        def task = new Task()
        def output = task.getNotify()

        then:
        assert output ==~ /^https:\/\/.*/
    }

    def "getMlbackendPythonVersion() with no mlbackendPythonVersion"() {
        given:

        when:
        def task = new Task()
        def output = task.getMlbackendPythonVersion()

        then:
        assert output == ''
    }

    def "getMlbackendPythonVersion() with null mlbackendPythonVersion"() {
        given:
        def mlbackendPythonVersion = null

        when:
        def task = new Task(mlbackendPythonVersion: mlbackendPythonVersion)
        def output = task.getMlbackendPythonVersion()

        then:
        assert output == ''
    }

    def "getMlbackendPythonVersion() with mlbackendPythonVersion"() {
        given:
        def mlbackendPythonVersion = 'python_version'

        when:
        def task = new Task(mlbackendPythonVersion: mlbackendPythonVersion)
        def output = task.getMlbackendPythonVersion()

        then:
        assert output == 'python_version'
    }

    def "getTimingFileName() with no behatTags"() {
        given:
        def behatTags = ''
        def behatSuite = ''

        when:
        def task = new Task(behatTags: behatTags, behatSuite: behatSuite)
        def output = task.getTimingFileName()

        then:
        assert output == 'default.json'
    }

    def "getTimingFileName() with classic behatSuite no behatTags"() {
        given:
        def behatTags = ''
        def behatSuite = 'classic'

        when:
        def task = new Task(behatTags: behatTags, behatSuite: behatSuite)
        def output = task.getTimingFileName()

        then:
        assert output == 'classic.json'
    }

    def "getTimingFileName() with @javascript tag"() {
        given:
        def behatTags = '@javascript'
        def behatSuite = ''

        when:
        def task = new Task(behatTags: behatTags, behatSuite: behatSuite)
        def output = task.getTimingFileName()

        then:
        assert output == 'default.json'
    }

    def "shouldNotify() with notify = false"() {
        given:
        def notify = false

        when:
        def task = new Task(notify: notify)
        def output = task.shouldNotify()

        then:
        assert output == false
    }

    def "getShaSum() with empty value"() {
        given:
        def value = ''

        when:
        def task = new Task()
        def output = task.getShaSum(value)

        then:
        assert output.length() == 40
    }

    def "getShaSum() with value"() {
        given:
        def value = 'test_value'

        when:
        def task = new Task()
        def output = task.getShaSum(value)

        then:
        assert output.length() == 40
        assert output == 'b6dfecf3684e6a40de435195509ac724c7349c03'
    }

    def "shouldNotify() with notify = null"() {
        given:
        def notify = null

        when:
        def task = new Task(notify: notify)
        def output = task.shouldNotify()

        then:
        assert output == false
    }

    def "shouldNotify() with notify = ''"() {
        given:
        def notify = ''

        when:
        def task = new Task(notify: notify)
        def output = task.shouldNotify()

        then:
        assert output == false
    }

    def "shouldNotify() with notify = 'https://example.com'"() {
        given:
        def notify = 'https://example.com'

        when:
        def task = new Task(notify: notify)
        def output = task.shouldNotify()

        then:
        assert output == true
    }

    def "databaseReplicaCount with replicas not specified"() {
        when:
        def task = new Task()
        def output = task.databaseReplicaCount

        then:
        assert output == 0
    }

    def "databaseReplicaCount with no replicas"() {
        given:
        def databaseReplicaCount = 0

        when:
        def task = new Task(databaseReplicaCount: databaseReplicaCount)
        def output = task.databaseReplicaCount

        then:
        assert output == 0
    }

    def "databaseReplicaCount with replicas"() {
        given:
        def databaseReplicaCount = 1

        when:
        def task = new Task(databaseReplicaCount: databaseReplicaCount)
        def output = task.databaseReplicaCount

        then:
        assert output == 1
    }

    def "databaseReplicaCount with too many replicas"() {
        given:
        def databaseReplicaCount = 2

        when:
        def task = new Task(databaseReplicaCount: databaseReplicaCount)
        def output = task.databaseReplicaCount

        then:
        assert output == 1
    }

    def "databaseReplicaCount with too few replicas"() {
        given:
        def databaseReplicaCount = -1

        when:
        def task = new Task(databaseReplicaCount: databaseReplicaCount)
        def output = task.databaseReplicaCount

        then:
        assert output == 0
    }

    def "getMobileAppBuildBranch() with no mobileAppBuildBranch"() {
        when:
        def task = new Task()
        def output = task.getMobileAppBuildBranch()

        then:
        assert output == ''
    }

    def "getMobileAppBuildBranch() with empty mobileAppBuildBranch"() {
        given:
        def mobileAppBuildBranch = ''

        when:
        def task = new Task(mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileAppBuildBranch()

        then:
        assert output == ''
    }

    def "getMobileAppBuildBranch() with mobileAppBuildBranch"() {
        given:
        def mobileAppBuildBranch = 'branch'

        when:
        def task = new Task(mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileAppBuildBranch()

        then:
        assert output == 'branch'
    }

    def "getMobileAppPort() with mobileAppPort gets precedence"() {
        given:
        def mobileAppPort = 1234
        def mobileAppBuildBranch = "master"

        when:
        def task = new Task(mobileAppPort: mobileAppPort, mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileAppPort()

        then:
        assert output == 1234
    }

    def "getMobileAppPort() without mobileAppPort or mobileAppBuildBranch port 8100"() {
        given:
        def mobileAppPort = null
        def mobileAppBuildBranch = ""

        when:
        def task = new Task(mobileAppPort: mobileAppPort, mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileAppPort()

        then:
        assert output == 8100
    }

    def "getMobileAppPort without mobileAppPort but with mobileAppBuildBranch port 80"() {
        given:
        def mobileAppPort = null
        def mobileAppBuildBranch = "master"

        when:
        def task = new Task(mobileAppPort: mobileAppPort, mobileAppBuildBranch: mobileAppBuildBranch)
        def output = task.getMobileAppPort()

        then:
        assert output == 80
    }

    def "getBrowser() with not browser specified"() {
        when:
        def task = new Task()

        then:
        assert task.getBrowserName() == ''
    }

    def "getBrowser() with browser specified"() {
        given:
        def browser = new Chrome()

        when:
        def task = new Task(browser: browser)

        then:
        assert task.getBrowserName() == 'chrome'
    }

    def "getBrowserHeadless() with not browser specified"() {
        when:
        def task = new Task()

        then:
        assert task.getBrowserHeadless() == 0
    }

    def "getBrowserHeadless() is not headless by default"() {
        given:
        def browser = new Chrome()

        when:
        def task = new Task(browser: browser)

        then:
        assert task.getBrowserHeadless() == 0
    }

    def "getBrowserHeadless() is converted to an int"() {
        given:
        def browser = new Chrome()
        browser.headless = true

        when:
        def task = new Task(browser: browser)

        then:
        assert task.getBrowserHeadless() == 1
    }

    def "getBrowserDebug() with no browser specified"() {
        when:
        def task = new Task()

        then:
        assert task.getBrowserDebug() == 0
    }

    def "getBrowserDebug() is not enabled by default"() {
        given:
        def browser = new Chrome()

        when:
        def task = new Task(browser: browser)

        then:
        assert task.getBrowserDebug() == 0
    }

    def "getBrowserDebug() is converted to an int"() {
        given:
        def browser = new Chrome()
        browser.debug = true

        when:
        def task = new Task(browser: browser)

        then:
        assert task.getBrowserDebug() == 1
    }

    def "getUrl() for a task where a https://github URL is used"() {
        given:
        def url = "https://github.com/moodle/moodle.git"

        when:
        def task = new Task(url: url)
        def output = task.getUrl()

        then:
        assert output == "https://github.com/moodle/moodle.git"
    }

    def "getUrl() for a task where a git://github URL is used"() {
        given:
        def url = "git://github.com/moodle/moodle.git"

        when:
        def task = new Task(url: url)
        def output = task.getUrl()

        then:
        assert output == "https://github.com/moodle/moodle.git"
    }

    def "getUrl() for a task where a git://some.other.host URL is used"() {
        given:
        def url = "git://some.other.host/moodle/moodle.git"

        when:
        def task = new Task(url: url)
        def output = task.getUrl()

        then:
        assert output == "git://some.other.host/moodle/moodle.git"
    }

    def "getUrl() uses the repository URL by default when url is not defined"() {
        given:
        def url = null
        def repository = new Security()

        when:
        def task = new Task(repository: repository, url: url)
        def output = task.getUrl()

        then:
        assert output == repository.url
    }

    def "getUrl() throws exception if neither url or repository are defined"() {
        given:
        def url = null
        def repository = null

        when:
        def task = new Task(repository: repository, url: url)
        def output = task.getUrl()

        then:
        def e = thrown(Exception)
        e.message == 'No repository specified.'
    }

    def "getCredentialsId() returns null if both credentialsId and repository are not set"() {
        given:
        def credentialsId = null
        def repository = null

        when:
        def task = new Task(repository: repository, credentialsId: credentialsId)
        def output = task.getCredentialsId()

        then:
        assert output == null
    }

    def "getCredentialsId() gives precedence to credentialsId over repository"() {
        given:
        def credentialsId = 'testCredential'
        def repository = new Security()

        when:
        def task = new Task(repository: repository, credentialsId: credentialsId)
        def output = task.getCredentialsId()

        then:
        assert output == 'testCredential'
    }

    def "getCredentialsId() returns repository credential when credentialsId is not set"() {
        given:
        def credentialsId = null

        when:
        def task = new Task(repository: repository, credentialsId: credentialsId)
        def output = task.getCredentialsId()

        then:
        assert output == repository.getCredentials()

        where:
        // Let's try all our repositories, so we have all them covered.
        repository << [
            new Repository(),
            new Security(),
            new Integration(),
            new Workplace(),
            new Qbank()
        ]
    }

    def "getCredentialsId() returns null if there aren't credentials in th repo"() {
        given:
        def credentialsId = null
        def repository = new Integration()

        when:
        def task = new Task(repository: repository, credentialsId: credentialsId)
        def output = task.getCredentialsId()

        then:
        assert output == null
    }

    def "getBranch() throws exception if both branch and moodleVersion are not set"() {
        given:
        def branch = null
        def moodleVersion = null

        when:
        def task = new Task(branch: branch, moodleVersion: moodleVersion)
        def output = task.getBranch()

        then:
        def e = thrown(Exception)
        e.message == 'No Pull URL set'
    }

    def "getBranch() gives precedence to branch over moodleVersion"() {
        given:
        def branch = 'test_branch'
        def moodleVersion = new M403()

        when:
        def task = new Task(branch: branch, moodleVersion: moodleVersion)
        def output = task.getBranch()

        then:
        assert output == 'test_branch'
    }

    def "getBranch() uses moodleVersion when branch is not set"() {
        given:
        def branch = null
        def moodleVersion = new M403()

        when:
        def task = new Task(branch: branch, moodleVersion: moodleVersion)
        def output = task.getBranch()

        then:
        assert output == 'MOODLE_403_STABLE'
    }

    def "getPhpVersion() with missing phpVersion and moodleVersion throws exception"() {
        given:
        def phpVersion = null
        def moodleVersion = null

        when:
        def task = new Task(phpVersion: phpVersion, moodleVersion: moodleVersion)
        def output = task.getPhpVersion()

        then:
        def e = thrown(Exception)
        e.message == 'I don\'t know what PHP Version to test against'
    }

    def "getPhpVersion() gives precedence to phpVersion over moodleVersion"() {
        given:
        def phpVersion = 'test_version'
        def moodleVersion = new M403()

        when:
        def task = new Task(phpVersion: phpVersion, moodleVersion: moodleVersion)
        def output = task.getPhpVersion()

        then:
        assert output == 'test_version'
    }

    def "getPhpVersion() returns the highest version when only moodleVersion is available"() {
        given:
        def phpVersion = null
        def moodleVersion = new M403()

        when:
        def task = new Task(phpVersion: phpVersion, moodleVersion: moodleVersion)
        def output = task.getPhpVersion()

        then:
        assert output == moodleVersion.getHighestSupportedVersion()
    }

    def "getPhpVersion() returns the lowest PHP version when phpVersion is LOWEST"() {
        given:
        def phpVersion = Task.LOWEST
        def moodleVersion = new M403()

        when:
        def task = new Task(phpVersion: phpVersion, moodleVersion: moodleVersion)
        def output = task.getPhpVersion()

        then:
        assert output == moodleVersion.getLowestSupportedVersion()
    }

    def "getPhpVersion() returns the highest PHP version when phpVersion is HIGHEST"() {
        given:
        def phpVersion = Task.HIGHEST
        def moodleVersion = new M403()

        when:
        def task = new Task(phpVersion: phpVersion, moodleVersion: moodleVersion)
        def output = task.getPhpVersion()

        then:
        assert output == moodleVersion.getHighestSupportedVersion()
    }

    def "seleniumVersion with default"() {
        given:

        when:
        def task= new Task()
        def returns = task.seleniumVersion

        then:
        assert returns == "latest"
    }

    def "seleniumVersion with older version"() {
        given:
        def seleniumVersion = "3.141.59"

        when:
        def task = new Task(seleniumVersion: seleniumVersion)
        def returns = task.seleniumVersion

        then:
        assert returns == "3.141.59"
    }

    def "Chrome and Selenium 3.x should use latest"() {
        given:
        def seleniumVersion = "3.141.59"
        def browser = new org.moodle.ci.browsers.Chrome();

        when:
        def task = new Task(browser: browser, seleniumVersion: seleniumVersion)
        def returns = task.getUseSeleniumVersion()

        then:
        assert returns == false
    }

    def "Firefox and Selenium 3.x should not use latest"() {
        given:
        def seleniumVersion = "3.141.59"
        def browser = new org.moodle.ci.browsers.Firefox();

        when:
        def task = new Task(browser: browser, seleniumVersion: seleniumVersion)
        def returns = task.getUseSeleniumVersion()

        then:
        assert returns == true
    }
}
