package org.moodle.ci.versions;

import spock.lang.Specification

class M37Spec extends Specification {
    def "getDefaultBranch returns MOODLE_37_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M37()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "MOODLE_37_STABLE"
    }

    def "defaultBranch returns MOODLE_37_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M37()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "MOODLE_37_STABLE"
    }

    def "getPhpVersions"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M37()

        when:
        def output = moodleVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.1', '7.2', '7.3'].sort()
    }

    def "getHighestSupportedVersion is 7.3"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M37()

        when:
        def output = moodleVersion.getHighestSupportedVersion()

        then:
        assert output == '7.3'
    }

    def "getLowestSupportedVersion is 7.1"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M37()

        when:
        def output = moodleVersion.getLowestSupportedVersion()

        then:
        assert output == '7.1'
    }

    def "getDatabases"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M37()

        when:
        def output = moodleVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M37()

        when:
        def output = moodleVersion.name

        then:
        assert output == "37"
    }
}
