package org.moodle.ci.versions;

import spock.lang.Specification

class WP38Spec extends Specification {
    def "getDefaultBranch returns WORKPLACE_38_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP38()

        when:
        def output = workplaceVersion.getDefaultBranch()

        then:
        assert output == "WORKPLACE_38_LATEST"
    }

    def "defaultBranch returns WORKPLACE_38_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP38()

        when:
        def output = workplaceVersion.defaultBranch

        then:
        assert output == "WORKPLACE_38_LATEST"
    }

    def "getPhpVersions"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP38()

        when:
        def output = workplaceVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.1', '7.2', '7.3', '7.4'].sort()
    }

    def "getHighestSupportedVersion is 7.4"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP38()

        when:
        def output = workplaceVersion.getHighestSupportedVersion()

        then:
        assert output == '7.4'
    }

    def "getLowestSupportedVersion is 7.1"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP38()

        when:
        def output = workplaceVersion.getLowestSupportedVersion()

        then:
        assert output == '7.1'
    }

    def "getDatabases"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP38()

        when:
        def output = workplaceVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP38()

        when:
        def output = workplaceVersion.name

        then:
        assert output == "38"
    }
}
