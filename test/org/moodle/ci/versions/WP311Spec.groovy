package org.moodle.ci.versions;

import spock.lang.Specification

class WP311Spec extends Specification {
    def "getDefaultBranch returns WORKPLACE_311_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP311()

        when:
        def output = workplaceVersion.getDefaultBranch()

        then:
        assert output == "WORKPLACE_311_LATEST"
    }

    def "defaultBranch returns WORKPLACE_311_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP311()

        when:
        def output = workplaceVersion.defaultBranch

        then:
        assert output == "WORKPLACE_311_LATEST"
    }

    def "getPhpVersions"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP311()

        when:
        def output = workplaceVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.3', '8.0'].sort()
    }

    def "getHighestSupportedVersion is 8.0"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP311()

        when:
        def output = workplaceVersion.getHighestSupportedVersion()

        then:
        assert output == '8.0'
    }

    def "getLowestSupportedVersion is 7.3"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP311()

        when:
        def output = workplaceVersion.getLowestSupportedVersion()

        then:
        assert output == '7.3'
    }

    def "getDatabases"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP311()

        when:
        def output = workplaceVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP311()

        when:
        def output = workplaceVersion.name

        then:
        assert output == "311"
    }
}
