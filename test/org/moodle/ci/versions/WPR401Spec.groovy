package org.moodle.ci.versions;

import spock.lang.Specification

class WPR401Spec extends Specification {
    def "getDefaultBranch returns WORKPLACE_ROLLING_401_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR401()

        when:
        def output = workplaceVersion.getDefaultBranch()

        then:
        assert output == "WORKPLACE_ROLLING_401_LATEST"
    }

    def "defaultBranch returns WORKPLACE_ROLLING_401_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR401()

        when:
        def output = workplaceVersion.defaultBranch

        then:
        assert output == "WORKPLACE_ROLLING_401_LATEST"
    }

    def "getPhpVersions"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR401()

        when:
        def output = workplaceVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.4', '8.0', '8.1'].sort()
    }

    def "getHighestSupportedVersion is 8.1"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR401()

        when:
        def output = workplaceVersion.getHighestSupportedVersion()

        then:
        assert output == '8.1'
    }

    def "getLowestSupportedVersion is 7.4"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR401()

        when:
        def output = workplaceVersion.getLowestSupportedVersion()

        then:
        assert output == '7.4'
    }

    def "getDatabases"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR401()

        when:
        def output = workplaceVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR401()

        when:
        def output = workplaceVersion.name

        then:
        assert output == "401"
    }
}
