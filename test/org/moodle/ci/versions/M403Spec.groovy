package org.moodle.ci.versions;

import spock.lang.Specification

class M403Spec extends Specification {
    def "getDefaultBranch returns MOODLE_403_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M403()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "MOODLE_403_STABLE"
    }

    def "defaultBranch returns MOODLE_403_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M403()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "MOODLE_403_STABLE"
    }

    def "getPhpVersions"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M403()

        when:
        def output = moodleVersion.getPhpVersions()

        then:
        assert output.sort() == ['8.0', '8.2'].sort()
    }

    def "getHighestSupportedVersion is 8.2"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M403()

        when:
        def output = moodleVersion.getHighestSupportedVersion()

        then:
        assert output == '8.2'
    }

    def "getLowestSupportedVersion is 8.0"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M403()

        when:
        def output = moodleVersion.getLowestSupportedVersion()

        then:
        assert output == '8.0'
    }

    def "getDatabases"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M403()

        when:
        def output = moodleVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M403()

        when:
        def output = moodleVersion.name

        then:
        assert output == "403"
    }
}
