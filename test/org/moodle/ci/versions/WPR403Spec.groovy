package org.moodle.ci.versions;

import spock.lang.Specification

class WPR403Spec extends Specification {
    def "getDefaultBranch returns WORKPLACE_ROLLING_403_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR403()

        when:
        def output = workplaceVersion.getDefaultBranch()

        then:
        assert output == "WORKPLACE_ROLLING_403_LATEST"
    }

    def "defaultBranch returns WORKPLACE_ROLLING_403_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR403()

        when:
        def output = workplaceVersion.defaultBranch

        then:
        assert output == "WORKPLACE_ROLLING_403_LATEST"
    }

    def "getPhpVersions"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR403()

        when:
        def output = workplaceVersion.getPhpVersions()

        then:
        assert output.sort() == ['8.0', '8.2'].sort()
    }

    def "getHighestSupportedVersion is 8.2"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR403()

        when:
        def output = workplaceVersion.getHighestSupportedVersion()

        then:
        assert output == '8.2'
    }

    def "getLowestSupportedVersion is 8.0"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR403()

        when:
        def output = workplaceVersion.getLowestSupportedVersion()

        then:
        assert output == '8.0'
    }

    def "getDatabases"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR403()

        when:
        def output = workplaceVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WPR403()

        when:
        def output = workplaceVersion.name

        then:
        assert output == "403"
    }
}
