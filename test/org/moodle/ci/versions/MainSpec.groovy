package org.moodle.ci.versions;

import spock.lang.Specification

class MainSpec extends Specification {
  def "getDefaultBranch returns main"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.Main()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "main"
    }

  def "defaultBranch returns main"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.Main()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "main"
    }
}
