package org.moodle.ci.versions;

import spock.lang.Specification

class M38Spec extends Specification {
    def "getDefaultBranch returns MOODLE_38_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M38()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "MOODLE_38_STABLE"
    }

    def "defaultBranch returns MOODLE_38_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M38()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "MOODLE_38_STABLE"
    }

    def "getPhpVersions"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M38()

        when:
        def output = moodleVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.1', '7.2', '7.3', '7.4'].sort()
    }

    def "getHighestSupportedVersion is 7.4"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M38()

        when:
        def output = moodleVersion.getHighestSupportedVersion()

        then:
        assert output == '7.4'
    }

    def "getLowestSupportedVersion is 7.1"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M38()

        when:
        def output = moodleVersion.getLowestSupportedVersion()

        then:
        assert output == '7.1'
    }

    def "getDatabases"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M38()

        when:
        def output = moodleVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M38()

        when:
        def output = moodleVersion.name

        then:
        assert output == "38"
    }
}
