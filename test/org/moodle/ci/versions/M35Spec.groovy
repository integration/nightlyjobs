package org.moodle.ci.versions;

import spock.lang.Specification

class M35Spec extends Specification {
    def "getDefaultBranch returns MOODLE_35_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M35()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "MOODLE_35_STABLE"
    }

    def "defaultBranch returns MOODLE_35_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M35()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "MOODLE_35_STABLE"
    }

    def "getPhpVersions"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M35()

        when:
        def output = moodleVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.0', '7.1', '7.2'].sort()
    }

    def "getHighestSupportedVersion is 7.2"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M35()

        when:
        def output = moodleVersion.getHighestSupportedVersion()

        then:
        assert output == '7.2'
    }

    def "getLowestSupportedVersion is 7.0"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M35()

        when:
        def output = moodleVersion.getLowestSupportedVersion()

        then:
        assert output == '7.0'
    }

    def "getDatabases"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M35()

        when:
        def output = moodleVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M35()

        when:
        def output = moodleVersion.name

        then:
        assert output == "35"
    }
}
