package org.moodle.ci.versions;

import spock.lang.Specification

class M404Spec extends Specification {
    def "getDefaultBranch returns MOODLE_404_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M404()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "MOODLE_404_STABLE"
    }

    def "defaultBranch returns MOODLE_404_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M404()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "MOODLE_404_STABLE"
    }

    def "getPhpVersions"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M404()

        when:
        def output = moodleVersion.getPhpVersions()

        then:
        assert output.sort() == ['8.1', '8.3'].sort()
    }

    def "getHighestSupportedVersion is 8.3"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M404()

        when:
        def output = moodleVersion.getHighestSupportedVersion()

        then:
        assert output == '8.3'
    }

    def "getLowestSupportedVersion is 8.1"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M404()

        when:
        def output = moodleVersion.getLowestSupportedVersion()

        then:
        assert output == '8.1'
    }

    def "getDatabases"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M404()

        when:
        def output = moodleVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M404()

        when:
        def output = moodleVersion.name

        then:
        assert output == "404"
    }
}
