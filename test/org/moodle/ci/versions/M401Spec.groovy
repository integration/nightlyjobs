package org.moodle.ci.versions;

import spock.lang.Specification

class M401Spec extends Specification {
    def "getDefaultBranch returns MOODLE_401_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M401()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "MOODLE_401_STABLE"
    }

    def "defaultBranch returns MOODLE_401_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M401()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "MOODLE_401_STABLE"
    }

    def "getPhpVersions"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M401()

        when:
        def output = moodleVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.4', '8.0', '8.1'].sort()
    }

    def "getHighestSupportedVersion is 8.1"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M401()

        when:
        def output = moodleVersion.getHighestSupportedVersion()

        then:
        assert output == '8.1'
    }

    def "getLowestSupportedVersion is 7.4"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M401()

        when:
        def output = moodleVersion.getLowestSupportedVersion()

        then:
        assert output == '7.4'
    }

    def "getDatabases"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M401()

        when:
        def output = moodleVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M401()

        when:
        def output = moodleVersion.name

        then:
        assert output == "401"
    }
}
