package org.moodle.ci.versions;

import spock.lang.Specification

class WP310Spec extends Specification {
    def "getDefaultBranch returns WORKPLACE_310_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP310()

        when:
        def output = workplaceVersion.getDefaultBranch()

        then:
        assert output == "WORKPLACE_310_LATEST"
    }

    def "defaultBranch returns WORKPLACE_310_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP310()

        when:
        def output = workplaceVersion.defaultBranch

        then:
        assert output == "WORKPLACE_310_LATEST"
    }

    def "getPhpVersions"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP310()

        when:
        def output = workplaceVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.2', '7.3', '7.4'].sort()
    }

    def "getHighestSupportedVersion is 7.4"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP310()

        when:
        def output = workplaceVersion.getHighestSupportedVersion()

        then:
        assert output == '7.4'
    }

    def "getLowestSupportedVersion is 7.2"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP310()

        when:
        def output = workplaceVersion.getLowestSupportedVersion()

        then:
        assert output == '7.2'
    }

    def "getDatabases"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP310()

        when:
        def output = workplaceVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP310()

        when:
        def output = workplaceVersion.name

        then:
        assert output == "310"
    }
}
