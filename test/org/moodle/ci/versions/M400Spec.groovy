package org.moodle.ci.versions;

import spock.lang.Specification

class M400Spec extends Specification {
    def "getDefaultBranch returns MOODLE_400_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M400()

        when:
        def output = moodleVersion.getDefaultBranch()

        then:
        assert output == "MOODLE_400_STABLE"
    }

    def "defaultBranch returns MOODLE_400_STABLE"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M400()

        when:
        def output = moodleVersion.defaultBranch

        then:
        assert output == "MOODLE_400_STABLE"
    }

    def "getPhpVersions"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M400()

        when:
        def output = moodleVersion.getPhpVersions()

        then:
        assert output.sort() == ['7.3', '8.0'].sort()
    }

    def "getHighestSupportedVersion is 8.0"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M400()

        when:
        def output = moodleVersion.getHighestSupportedVersion()

        then:
        assert output == '8.0'
    }

    def "getLowestSupportedVersion is 7.3"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M400()

        when:
        def output = moodleVersion.getLowestSupportedVersion()

        then:
        assert output == '7.3'
    }

    def "getDatabases"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M400()

        when:
        def output = moodleVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def moodleVersion = new org.moodle.ci.versions.M400()

        when:
        def output = moodleVersion.name

        then:
        assert output == "400"
    }
}
