package org.moodle.ci.versions;

import spock.lang.Specification

class WP404Spec extends Specification {
    def "getDefaultBranch returns WORKPLACE_404_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP404()

        when:
        def output = workplaceVersion.getDefaultBranch()

        then:
        assert output == "WORKPLACE_404_LATEST"
    }

    def "defaultBranch returns WORKPLACE_404_LATEST"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP404()

        when:
        def output = workplaceVersion.defaultBranch

        then:
        assert output == "WORKPLACE_404_LATEST"
    }

    def "getPhpVersions"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP404()

        when:
        def output = workplaceVersion.getPhpVersions()

        then:
        assert output.sort() == ['8.1', '8.3'].sort()
    }

    def "getHighestSupportedVersion is 8.3"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP404()

        when:
        def output = workplaceVersion.getHighestSupportedVersion()

        then:
        assert output == '8.3'
    }

    def "getLowestSupportedVersion is 8.1"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP404()

        when:
        def output = workplaceVersion.getLowestSupportedVersion()

        then:
        assert output == '8.1'
    }

    def "getDatabases"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP404()

        when:
        def output = workplaceVersion.getDatabases()

        then:
        assert output.sort() == [
                "mariadb",
                "mysqli",
                "oci",
                "pgsql",
                "sqlsrv",
            ].sort()
    }

    def "name matches"() {
        given:
        def workplaceVersion = new org.moodle.ci.versions.WP404()

        when:
        def output = workplaceVersion.name

        then:
        assert output == "404"
    }
}
