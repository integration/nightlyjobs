package org.moodle.ci.browsers;

import spock.lang.Specification

class FirefoxSpec extends Specification {
    def "Default tags are @javascript and ~@skip_interim"() {
        given:
        def browser = new org.moodle.ci.browsers.Firefox();

        when:
        def output = browser.getDefaultTags()

        then:
        assert output == "@javascript&&~@skip_interim"
    }

    def "Name is firefox"() {
        given:
        def browser = new org.moodle.ci.browsers.Firefox();

        when:
        def output = browser.name

        then:
        assert output == "firefox"
    }

    def "Headless is respected"() {
        given:
        def browser = new org.moodle.ci.browsers.Chrome();

        when:
        browser.headless = true

        then:
        assert browser.headless
    }

    def "Debug is respected"() {
        given:
        def browser = new org.moodle.ci.browsers.Chrome();

        when:
        browser.debug = true

        then:
        assert browser.debug
    }
}
