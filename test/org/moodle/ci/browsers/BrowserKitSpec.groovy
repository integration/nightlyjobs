package org.moodle.ci.browsers;

import spock.lang.Specification

class BrowserKitSpec extends Specification {
    def "Default tags are ~@javascript and ~@skip_interim"() {
        given:
        def browser = new org.moodle.ci.browsers.BrowserKit();

        when:
        def output = browser.getDefaultTags()

        then:
        assert output == "~@javascript&&~@skip_interim"
    }

    def "Name is browserkit"() {
        given:
        def browser = new org.moodle.ci.browsers.BrowserKit();

        when:
        def output = browser.name

        then:
        assert output == "browserkit"
    }
}
