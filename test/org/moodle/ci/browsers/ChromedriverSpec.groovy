package org.moodle.ci.browsers;

import spock.lang.Specification

class ChromedriverSpec extends Specification {
    def "Default tags are @javascript, ~@skip_chrome_zerosize and ~@skip_interim"() {
        given:
        def browser = new org.moodle.ci.browsers.Chromedriver();

        when:
        def output = browser.getDefaultTags()

        then:
        assert output == "@javascript&&~@skip_chrome_zerosize&&~@skip_interim"
    }

    def "Name is chromedriver"() {
        given:
        def browser = new org.moodle.ci.browsers.Chromedriver();

        when:
        def output = browser.name

        then:
        assert output == "chromedriver"
    }

    def "Headless is respected"() {
        given:
        def browser = new org.moodle.ci.browsers.Chromedriver();

        when:
        browser.headless = true

        then:
        assert browser.headless
    }

    def "Debug is respected"() {
        given:
        def browser = new org.moodle.ci.browsers.Chromedriver();

        when:
        browser.debug = true

        then:
        assert browser.debug
    }
}
